angular.module('starter.controllers', [])
        .controller('TabsCtrl', function ($scope, $rootScope, $state, $ionicPopover, $window) {

            // This is to hide the tabs on the login/signup pages
            $rootScope.$on('$ionicView.beforeEnter', function () {
                $rootScope.hideTabs = false;
                if ($state.current.name === 'main.login') {
                    $rootScope.hideTabs = true;
                }
                if ($state.current.name === 'main.intro') {
                    $rootScope.hideTabs = true;
                }

                $scope.leftButtons = [{
                        type: 'button-icon icon ion-navicon',
                        tap: function (e) {
                            $scope.toggleMenu();
                        }
                    }];
            });
        })
        .controller('LoginCtrl', function ($scope, $rootScope, $ionicModal, $state, $window, AuthFactory, GlobalServices, $ionicPopup, usSpinnerService, errorMsg) {
            // var geocoder = new google.maps.Geocoder();

            $rootScope.firstLogin = true;

            $scope.currentDate = new Date();
            console.log('currentDate');
            $scope.currentYear = $scope.currentDate.getFullYear();
            $scope.login = {
                username: "",
                password: ""
            };
            $scope.fp = {
                fpemail: ""
            };
            $scope.signup = {
                password: "",
                password1: "",
                email: "",
                firstname: "",
                lastname: "",
                phone_no: "",
                gender: "",
                location: "",
                dob_month: "",
                dob_day: "",
                dob_year: "",
                dob: "",
                country: "",
                // timezone: ""
            };

            usSpinnerService.stop('spinner-1');

            $scope.startSpin = function () {
                usSpinnerService.spin('spinner-1');
            };

            $scope.showSignupErrors = false;
            // $scope.signUp = function() {
            //   $state.go('main.dash');
            //   $scope.closeModal();
            // };

            $scope.phoneNumberError = false;
            $scope.signup.validatePhoneNum = function (key) {

                if (key == 1) {
                    if ($scope.signup.mobNumberFirst.length == 3) {
                        var element = document.getElementById('mob_num_input_second');
                        element.focus();
                    } else if ($scope.signup.mobNumberFirst.length > 3) {
                        $scope.signup.mobNumberFirst = $scope.signup.mobNumberFirst.substring(0, 3);
                        var element = document.getElementById('mob_num_input_second');
                        element.focus();
                    }
                } else if (key == 2) {
                    if ($scope.signup.mobNumberSecond.length == 3) {
                        var element = document.getElementById('mob_num_input_third');
                        element.focus();
                    } else if ($scope.signup.mobNumberSecond.length > 3) {
                        $scope.signup.mobNumberSecond = $scope.signup.mobNumberSecond.substring(0, 3);
                        var element = document.getElementById('mob_num_input_third');
                        element.focus();
                    }
                } else if (key == 3) {
                    if ($scope.signup.mobNumberThird.length > 4) {
                        $scope.signup.mobNumberThird = $scope.signup.mobNumberThird.substring(0, 4);
                    }
                }

                $scope.signup.phone = $scope.signup.mobNumberFirst + $scope.signup.mobNumberSecond + $scope.signup.mobNumberThird;

            };

            $scope.range = function (from, to) {
                var temp = [];
                if (from == 'now') {
                    from = $scope.currentYear;
                }
                if (from > to) {
                    for (i = from; i >= to; i--) {
                        temp.push(i);
                    }
                } else {
                    for (i = from; i <= to; i++) {
                        temp.push(i);
                    }
                }
                return temp;
            };


            $scope.login = function () {
                var data = {
                    "username": angular.lowercase($scope.login.username),
                    "password": $scope.login.password
                };
                AuthFactory.login(data).then(function (response) {
                    console.log(response);
                    if (response.data.status == "success") {
                        localStorage.setItem("userDetails", JSON.stringify(response.data));
                        GlobalServices.username = response.data.username;
                        GlobalServices.user_name = response.data.user_name;
                        var names = response.data.user_name.split(' ');
                        GlobalServices.firstname = names[0];
                        GlobalServices.lastname = names[1];
                        GlobalServices.id = response.data._id;
                        console.log(GlobalServices.id);
                        $state.go('main.menu');
                        $scope.closeModal();
                    } else if (response.data.status == "fail") {
                        // $('#login_unauthorized').fadeIn();
                        $ionicPopup.alert({
                            title: "Error",
                            template: response.data.message
                        });
                    }
                });
            };


            $scope.signup.validateDate = function (key) {

                var month = 0, day = 0, year = 0;

                if ($scope.signup.dob_month !== undefined)
                    month = $scope.signup.dob_month.toString();
                if ($scope.signup.dob_day !== undefined)
                    day = $scope.signup.dob_day.toString();
                if ($scope.signup.dob_year !== undefined)
                    year = $scope.signup.dob_year.toString();

                if (month.length === 2 && key === 'month') {
                    var element = document.getElementById('dob_day');
                    element.focus();
                }

                if (day.length === 2 && key === 'day') {
                    var element = document.getElementById('dob_year');
                    element.focus();
                }

                if (month.length > 2) {
                    month = month.substring(0, 2);
                    $scope.signup.dob_month = parseInt(month);
                }
                if (day.length > 2) {
                    day = day.substring(0, 2);
                    $scope.signup.dob_day = parseInt(day);
                    var element = document.getElementById('dob_year');
                    element.focus();
                }
                if (year.length > 4) {
                    year = year.substring(0, 4);
                    $scope.signup.dob_year = parseInt(year);
                } else if (year.length === 4) {
                    $scope.formYearError = false;
                }

                if ($scope.signup.dob_month < 1 && month.length > 1)
                    $scope.signup.dob_month = 1;
                if ($scope.signup.dob_day < 1 && day.length > 1)
                    $scope.signup.dob_day = 1;
                if ($scope.signup.dob_year < 1901 && year.length == 4)
                    $scope.signup.dob_year = 1901;

                if ($scope.signup.dob_month > 12)
                    $scope.signup.dob_month = 12;
                if ($scope.signup.dob_day > 31)
                    $scope.signup.dob_day = 31;
                if ($scope.signup.dob_year > 2016)
                    $scope.signup.dob_year = 2016;
            };

            // flag to check if need to highlight not valid fields
            $scope.highlightInvalid = false;

            $scope.needHighlighting = function (isFormInvalid) {

                if ($scope.highlightInvalid)
                    $scope.showSignupErrors = true;

                return $scope.highlightInvalid && isFormInvalid;
            };

            $scope.signupapi = function (isFormValid) {
                var year;
//                var reMob = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;

//                if ($scope.signup.phone.length == 10 && reMob.test($scope.signup.phone)) {
//                    $scope.phoneNumberError = false;
//                } else
//                    $scope.phoneNumberError = true;

                if ($scope.signup.dob_year !== undefined)
                    year = $scope.signup.dob_year.toString();

                if (year != undefined && year.length < 4 && year.length > 0) {
                    isFormValid = false;
                    $scope.formYearError = true;
                }

                if (!isFormValid) {
                    $scope.highlightInvalid = true;
                    var tpl = angular.element('.error-section');
                    var confirmPopup = $ionicPopup.confirm({
                        title: 'Errors',
                        template: tpl
                    });
                    return;
                } else
                    $scope.highlightInvalid = false;

                if ($scope.signup.password != $scope.signup.password1) {
                    // alert("Both Passwords should match. ");
                    var valalert = $ionicPopup.alert({
                        title: "Error",
                        template: "Both Passwords should match."
                    });
                    valalert.then(function (res) {
                        return false;
                    });
                }
                var str = $scope.signup.dob;
                var dob = str.substr(0, 2) + '/' + str.substr(2, 2) + '/' + str.substr(4, 4);
//               var d = new Date($scope.signup.dob_year, $scope.signup.dob_month - 1, $scope.signup.dob_day);
                var d = moment(dob).format('ddd MMM DD YYYY');

                var data = {
                    "password": $scope.signup.password,
                    "email": angular.lowercase($scope.signup.email),
                    "firstname": $scope.signup.firstname,
                    "lastname": $scope.signup.lastname,
                    "phone_no": $scope.signup.phone,
                    "gender": $scope.signup.gender,
                    "location": $scope.signup.location,
                    "dateofbirth": d
                            // "country": "",
                            // "timezone": ""
                };
                var email = angular.lowercase($scope.signup.email);

                console.log(data);
                AuthFactory.signup(data).then(function (response) {
                    console.log(response);
                    if (response.data.status == "success") {
                        $scope.signup = {};

                        $scope.signup.showVerifyPopup = function (subtitle) {
                            var myVerifyPopup = $ionicPopup.show({
                                template: '<input type="password" ng-model="signup.verifyCode">',
                                title: 'Enter verification code',
                                subTitle: subtitle,
                                scope: $scope,
                                buttons: [
                                    {text: 'Cancel'},
                                    {
                                        text: '<b>Save</b>',
                                        type: 'button-positive',
                                        onTap: function (e) {
                                            if (!$scope.signup.verifyCode) {
                                                e.preventDefault();
                                            } else {
                                                return $scope.signup.verifyCode;
                                            }
                                        }
                                    }
                                ]
                            });


                            myVerifyPopup.then(function (res) {
                                console.log('Tapped!', res);

                                AuthFactory.verifyEmail({code: parseInt(res), email: email})
                                        .then(function (response) {
                                            console.log(response);
                                            if (response.data.status == "success") {
                                                $ionicPopup.alert({
                                                    title: "Success",
                                                    template: "Your email has successfully been verified"
                                                }).then(function () {
                                                    $state.go('login');
                                                    $scope.closeModal();
                                                });
                                            } else {
                                                $scope.signup.verifyCode = '';
                                                $scope.signup.showVerifyPopup('You entered false code. Please re-enter again.');
                                            }

                                        });
                            });
                        };

                        $scope.signup.showVerifyPopup('Your account has successfully been created. We sent a verification code to your email address. To verify your email please enter verification code.');

                    } else if (response.data.status == "fail") {
                        // alert("That username is already taken.");
                        $ionicPopup.alert({
                            title: "Error",
                            template: response.data.message
                        });
                    }
                });
            };
            $scope.datePickerCallback = function (val) {

                //$scope.dob = val;
                if (typeof (val) === 'undefined') {
                    console.log('Date not selected');
                } else {
                    console.log('Selected date is : ', val);
                    var newDate = new Date(val);
                    console.log(typeof val);
                    $scope.signup.dob = newDate.toString("dd/MMM/yyyy");
                    console.log("hi there");
                }
            };

            $scope.forgotPassword = function () {
                AuthFactory.forgotPassword({'email': $scope.fp.fpemail}).then(function (response) {
                    if (response.data.status == "success") {
                        $ionicPopup.alert({
                            title: "Success",
                            template: response.data.message
                        }).then(function () {
                            $scope.fp.fpemail = "";
                            $scope.modalPass.hide();
                            $state.go('login');
                        });
                    } else if (response.data.status == "error") {
                        $ionicPopup.alert({
                            title: "Error",
                            template: response.data.message
                        });
                    }
                });
            }

            $ionicModal.fromTemplateUrl('templates/signup.html', {
                scope: $scope,
                // animation: 'slide-in-up',
                animation: 'slide-in-right',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modal = modal;
            });
            $ionicModal.fromTemplateUrl('templates/forget-pass.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalPass = modal;
            });


            $scope.openModal = function (e) {
                e.preventDefault();
                $scope.modal.show();
            };
            $scope.openPass = function (e) {
                e.preventDefault();
                $scope.modalPass.show();
            };
            $scope.closeModal = function () {
                $scope.modal.hide();
            };
            $scope.closePass = function () {
                $scope.modalPass.hide();
            };

            //Cleanup the modal when we're done with it!
            $scope.$on('$destroy', function () {
                $scope.modal.remove();
                $scope.modalPass.remove();
            });
            // Execute action on hide modal
            $scope.$on('modal.hidden', function () {
                // Execute action
            });
            // Execute action on remove modal
            $scope.$on('modal.removed', function () {
                // Execute action
            });
        })
// .controller('SignupCtrl', function($scope, $ionicModal, $state, $window, AuthFactory, GlobalServices) {
//   $scope.currentDate = new Date();

//  $scope.datePickerCallback = function (val) {

//     //$scope.dob = val;
//       if(typeof(val)==='undefined'){
//           console.log('Date not selected');
//       }else{
//           console.log('Selected date is : ', val);
//           var newDate = new Date(val);

//       }
//   };

//   $ionicModal.fromTemplateUrl('templates/signup.html', {
//     scope: $scope,
//     // animation: 'slide-in-up',
//     animation: 'slide-in-right',
//     hardwareBackButtonClose: true
//   }).then(function(modal) {
//     $scope.modal = modal;
//   });
//   $ionicModal.fromTemplateUrl('templates/forget-pass.html', {
//     scope: $scope,
//     animation: 'slide-in-up',
//     hardwareBackButtonClose: true
//   }).then(function(modal) {
//     $scope.modalPass = modal;
//   });

//   $scope.openModal = function() {
//     $scope.modal.show();
//   };
//   $scope.openPass = function() {
//     $scope.modalPass.show();
//   };
//   $scope.closeModal = function() {
//     $scope.modal.hide();
//   };
//   $scope.closePass = function() {
//     $scope.modalPass.hide();
//   };
//   //Cleanup the modal when we're done with it!
//   $scope.$on('$destroy', function() {
//     $scope.modal.remove();
//     $scope.modalPass.remove();
//   });
//   // Execute action on hide modal
//   $scope.$on('modal.hidden', function() {
//     // Execute action
//   });
//   // Execute action on remove modal
//   $scope.$on('modal.removed', function() {
//     // Execute action
//   });
// })
        .controller('DashCtrl', function ($scope, $state, NewsFeedFactory) {

            $scope.newsFeed = {};

            $scope.leftButtons = [{
                    type: 'button-icon icon ion-navicon',
                    tap: function (e) {
                        $scope.toggleMenu();
                    }
                }];

            NewsFeedFactory.getNews().then(function (response) {
                console.log(response);
                for (var data in response.data) {
                    var title = document.createElement("textarea");
                    var content = document.createElement("textarea");
                    title.innerHTML = response.data[data].title;
                    content.innerHTML = response.data[data].content;
                    response.data[data].title = title.value;
                    response.data[data].content = content.value;

                }

                $scope.newsFeed.lastNews = response.data;
            });

            //Correct format Date mm-dd-year
            $scope.date_format = function (date) {
                var y = new Date(date);
                var monthsList = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
                return "" + monthsList[y.getMonth()] + "-" + ("0" + y.getDay()).slice(-2) + "-" + y.getFullYear();
            };


        })
        .controller('MainMenuCtrl', function ($scope, $state, $rootScope, $ionicTabsDelegate, $timeout, GlobalServices, PrivateMessages) {
            $scope.interactSecondPage = function () {
                $rootScope.interact.secondPage();
            };
            $scope.interactFirstPage = function () {
                $rootScope.interact.firstPage();
            };
            $scope.goToFriends = function () {
                $state.go('main.friends');
            };
//            $scope.goToGroups = function () {
//                $state.go('main.groups');
//            };
//            $scope.goToConnection = function () {
//                $state.go('main.connection');
//            };
            $scope.page = 0;
            $scope.userName = GlobalServices.user_name;
            $scope.fName = GlobalServices.firstname;

            PrivateMessages.getInitiatedChats({}).then(function (response) {
                if (response.data) {
                    console.log(response);
                    $scope.unreadCount = response.data.unreadcnt;
                }

            });



            $ionicTabsDelegate.showBar(false);
        })
        .controller('PlanCtrl', function ($scope, $state, $ionicModal, $ionicPopup, FriendFactory, EventFactory, GlobalServices, DateTimeServices) {
            $ionicModal.fromTemplateUrl('templates/add-event.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modal = modal;
            });

            $ionicModal.fromTemplateUrl('templates/privacy-event.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalPrivacy = modal;
            });

            $ionicModal.fromTemplateUrl('templates/edit-event.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalEdit = modal;
            });

            $ionicModal.fromTemplateUrl('templates/status-event.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalStatus = modal;
            });

            $scope.editMode = false;

            $scope.openDatePicker = function (event) {
                var date = 0;
                if ((typeof (event.displayDate) !== "undefined" && typeof (event.displayDate) !== "object" && typeof (event.displayDate) !== "string") || $scope.editMode) {
                    date = new Date(event.displayDate);
                } else {
                    date = new Date();
                }
                DateTimeServices.openDatePicker(date, function (val, selectedDate) {
//                    event.showDate = GlobalServices.getDateFromDateTime(selectedDate);
                    event.showDate = moment(selectedDate).format('MM/DD/YYYY');
                    event.displayDate = val;
                });
            }

            $scope.openTimePicker = function (event) {
                var time = ((new Date()).getHours() * 60 * 60 + (new Date()).getMinutes() * 60);
                if (typeof (event.dateTime) == 'object' && !$scope.editMode) {
                    time = 0;
                } else if (typeof (event.dateTime) == 'object' && $scope.editMode) {
                    // time = event.dateTime.getTime();
                    var temp = angular.copy(event.dateTime);
                    // temp.setDate(1);
                    // temp.setMonth(1);
                    // temp.setFullYear(1970);
                    var hrs = temp.getHours();                    
                    var mins = temp.getMinutes();
                    time = (hrs * 60 * 60) + (mins * 60);
                    //time = temp.getTime() / 1000;
                } else {
                    time = event.dateTime;
                }

                DateTimeServices.openTimePicker(time, function (val, selectedTime) {
                    event.dateTime = val;
                    var hrs = selectedTime.getUTCHours() <= 9 ? "0" + selectedTime.getUTCHours().toString() : selectedTime.getUTCHours().toString();
                    var mins = selectedTime.getUTCMinutes() <= 9 ? "0" + selectedTime.getUTCMinutes().toString() : selectedTime.getUTCMinutes().toString();
                    event.showTime = hrs + ":" + mins;
                });
            }

            $scope.showErrors = false;

            $scope.userProfile = GlobalServices.id;

            $scope.page = 0;

            $scope.moreCanBeLoaded = true;

            $scope.eventsList = [];

            $scope.events = {};

            $scope.formatDate = function (current) {
                var monthsList = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"]
                return monthsList[current.getMonth()] + " - " + ("0" + current.getDay()).slice(-2) + " - " + current.getFullYear();
            };




            FriendFactory.getUserGroups().then(function (response) {
                if (response.data) {

                    $scope.listG = response.data;
                }
            });

            $scope.getItems = function (query) {

                var result = [];


                for (var i = 0; i < $scope.listF.length; i++) {
                    console.log($scope.listF[i].name + '\n' + $scope.listF[i].email);
                    var name = $scope.listF[i].name;
                    var email = $scope.listF[i].email;

                    if (name.toLowerCase().search(query.toLowerCase()) !== -1) {
                        var newItem = {name: name, email: email};
                        result.push(newItem);
                    }
                }
                ;

                console.log('Return: ' + result);
                return result;

            };

            $scope.getGroupItems = function (query) {

                var result = [];

                for (var i = 0; i < $scope.listG.length; i++) {
                    console.log($scope.listG[i].name + '\n' + $scope.listG[i]._id);
                    var name = $scope.listG[i].name;
                    var id = $scope.listG[i]._id;

                    if (name.toLowerCase().search(query.toLowerCase()) !== -1) {
                        var newItem = {name: name, _id: id};
                        result.push(newItem);
                    }
                }
                ;

                console.log('Return: ' + result);
                return result;

            };


            $scope.mapInit = function (index) {
                //console.log(index);

                if ($scope.eventsList[index].location.coordinates != undefined && $scope.eventsList[index].location.coordinates.lat != undefined)
                    var myLatlng = new google.maps.LatLng($scope.eventsList[index].location.coordinates.lat, $scope.eventsList[index].location.coordinates.lng);


                var mapOptions = {
                    center: myLatlng,
                    zoom: 8,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                var map = new google.maps.Map(document.getElementById("googleMap" + index), mapOptions);

                var marker = new google.maps.Marker({
                    position: myLatlng,
                    map: map
                });
                //console.log('bbb');
            }

            FriendFactory.getFriends().then(function (response) {
                if (response.data) {
                    $scope.listF = response.data;
                }
            });

            $scope.refreshEvents = function () {
                EventFactory.getEvents({'page': $scope.page}).then(function (response) {
                    if (response.data) {
                        console.log(response.data);
                        // $scope.eventsList = $scope.eventsList.concat(response.data);
                        if (response.data.length > 0) {
                            $scope.page++;
                        } else {
                            $scope.moreCanBeLoaded = false;
                        }
                        $scope.$broadcast('scroll.infiniteScrollComplete');
                        var exists = false;
                        for (var j in response.data) {
                            exists = false;
                            for (var i in $scope.eventsList) {
                                if ($scope.eventsList[i]._id == response.data[j]._id) {
                                    exists = true;
                                    break;
                                }
                            }
                            if (!exists) {
                                response.data[j].dateTime = response.data[j].dateTime + GlobalServices.getTimeZoneOffset();
                                $scope.eventsList.push(response.data[j]);
                            }
                        }
                        //$scope.mapInit();
                    }
                });
            }

            $scope.addNewEvent = function () {
                $scope.showErrors = true;
                if ($scope.newEvent.title && $scope.newEvent.location) {
                    var sharedFriends = [];
                    var sharedGroups = [];

                    if ($scope.newEvent.invite == 1) {
                        $scope.newEvent.friends = [];
                        $scope.newEvent.groups = [];
                    } else if ($scope.newEvent.invite == 2)
                        $scope.newEvent.friends = [];
                    else if ($scope.newEvent.invite == 3)
                        $scope.newEvent.groups = [];

                    for (var friend in $scope.newEvent.friends) {
                        for (var f in $scope.listF) {
                            if ($scope.newEvent.friends[friend] == $scope.listF[f].email) {
                                sharedFriends.push($scope.listF[f]);
                                break;
                            }
                        }
                    }
                    for (var group in $scope.newEvent.groups) {
                        for (var g in $scope.listF) {
                            if ($scope.newEvent.groups[group] == $scope.listG[g]._id) {
                                sharedGroups.push($scope.listG[g]);
                                break;
                            }
                        }
                    }

                    var myLatlng = $scope.newEvent.location.geometry.location;

                    var location = {
                        "coordinates": myLatlng,
                        "formatted_address": $scope.newEvent.location.formatted_address
                    };
                    var dateTime = new Date($scope.newEvent.displayDate);
                    var time = $scope.newEvent.showTime.split(":");
                    dateTime.setHours(time[0]);
                    dateTime.setMinutes(time[1]);
                    var data = {
                        username: GlobalServices.username,
                        title: $scope.newEvent.title,
                        location: location,
                        dateTime: dateTime.getTime() - GlobalServices.getTimeZoneOffset(),
                        sh_op: parseInt($scope.newEvent.invite),
                        sh_groups: sharedGroups,
                        sh_inds: sharedFriends,
                        sh_visib: parseInt($scope.newEvent.visib)
                    };
                    console.log(data);

                    EventFactory.addEvent(data).then(function (response) {
                        console.log(response);
                        if (response.data.status == "success") {
                            //success
                            $scope.addEventClose();
                            // $scope.refreshEvents();
                            $state.reload();
                        } else if (response.data.status == "unauthorized") {
                            //not log in
                        }
                    });
                }
            }
            $scope.addEventOpen = function () {
                $scope.currentDate = new Date();
                $scope.currentDate.setHours(0, 0, 0, 0);
                $scope.newEvent = {
                    title: "",
                    location: "",
                    dateTime: $scope.currentDate,
                    displayDate: moment().format('MMM-DD-YYYY'),
                    invite: ""
                };
                $scope.modal.show();
            };

            $scope.addEventClose = function () {
                $scope.modal.hide();
            };

            $scope.saveEvent = function () {
                if ($scope.editEvent.title && $scope.editEvent.location) {
                    var myLatlng;
                    if ($scope.editEvent.location.geometry !== undefined) {
                        myLatlng = $scope.editEvent.location.geometry.location;
                    } else {
                        myLatlng = $scope.editEvent.location.coordinates;
                    }

                    var location = {
                        "coordinates": myLatlng,
                        "formatted_address": $scope.editEvent.location.formatted_address
                    };

                    var sharedFriends = [], sharedGroups = [];

                    if ($scope.editEvent.invite == 1) {
                        $scope.editEvent.friends = [];
                        $scope.editEvent.groups = [];
                    } else if ($scope.editEvent.invite == 2)
                        $scope.editEvent.friends = [];
                    else if ($scope.editEvent.invite == 3)
                        $scope.editEvent.groups = [];

                    for (var friend in $scope.editEvent.friends) {
                        for (var f in $scope.listF) {
                            if ($scope.editEvent.friends[friend] == $scope.listF[f].email) {
                                sharedFriends.push($scope.listF[f]);
                                break;
                            }
                        }
                    }
                    for (var group in $scope.editEvent.groups) {
                        for (var g in $scope.listG) {
                            if ($scope.editEvent.groups[group] == $scope.listG[g]._id) {
                                var group = {"_id": $scope.listG[g]._id, "name": $scope.listG[g].name};
                                sharedGroups.push(group);
                                break;
                            }
                        }
                    }

                    var data = {
                        username: GlobalServices.username,
                        _id: $scope.editEvent._id,
                        title: $scope.editEvent.title,
                        location: location,
                        dateTime: dateTime.getTime() - GlobalServices.getTimeZoneOffset(),
                        sh_op: parseInt($scope.editEvent.invite),
                        sh_groups: sharedGroups,
                        sh_inds: sharedFriends,
                        sh_visib: parseInt($scope.editEvent.sh_visib)
                    };
                    console.log(data);
                    EventFactory.editEvent(data).then(function (response) {
                        console.log(response);
                        if (response.data.status == "success") {
                            //success
                            $scope.editEventClose();
                            // $scope.refreshEvents();
                            $state.reload();
                        } else if (response.data.status == "unauthorized") {
                            //not log in
                        }
                        $scope.editMode = false;
                    });
                }
            };

            $scope.modelToItemMethodGroup = function (modelValue) {

                for (var i = 0; i < $scope.listG.length; i++) {
                    if (modelValue == $scope.listG[i]._id) {
                        var name = $scope.listG[i].name;
                        var id = $scope.listG[i]._id;

                        return {name: name, _id: id};
                    }
                }
            };

            $scope.modelToItemMethodFriend = function (modelValue) {

                for (var i = 0; i < $scope.listF.length; i++) {
                    if (modelValue == $scope.listF[i].email) {
                        var name = $scope.listF[i].name;
                        var email = $scope.listF[i].email;

                        return {name: name, email: email};

                    }
                }

            };

            $scope.deleteEvent = function (index) {

                $ionicPopup.confirm({
                    title: 'Delete plan',
                    template: 'Do you want to delete this plan?'
                }).then(function (res) {
                    if (res) {
                        EventFactory.deleteEvent({'eid': $scope.eventsList[index]._id}).then(function (response) {
                            console.log(reponse);
                            $scope.refreshEvents();
                        });
                    }
                });

            }

            $scope.subscribeEvent = function (index) {
                EventFactory.subscribeEventReq({'eid': $scope.eventsList[index]._id}).then(function (response) {
                    console.log(response);
                    $scope.refreshEvents();
                });
            }

            $scope.acceptEvent = function (index) {
                EventFactory.acceptEventReq({'eid': $scope.eventsList[index]._id}).then(function (response) {
                    console.log(response);
                    $scope.refreshEvents();
                });
            }

            $scope.declineEvent = function (index) {

                EventFactory.declineEventReq({'eid': $scope.eventsList[index]._id}).then(function (response) {
                    console.log(response);
                    $scope.refreshEvents();
                });

            }

            $scope.statusEvent = function (index) {
                EventFactory.getEventDet({'eid': $scope.eventsList[index]._id}).then(function (response) {
                    $scope.events.currentEvenData = response.data;
                    $scope.modalStatusOpen();
                    console.log(response.data);
                });
            }

            $scope.editEventOpen = function (index) {
                $scope.editMode = true;
                EventFactory.getEventDet({'eid': $scope.eventsList[index]._id}).then(function (response) {
                    var eventData = response.data;
                    console.log(eventData);
                    var shGroups = [], shFriends = [];

                    for (var group in eventData.sh_groups) {
                        shGroups.push(eventData.sh_groups[group]._id);
                    }

                    for (var friend in eventData.sh_inds) {
                        shFriends.push(eventData.sh_inds[friend].email);
                    }

                    console.log(shGroups, shFriends);

                    var date = new Date(eventData.dateTime + GlobalServices.getTimeZoneOffset());
                    var hrs = date.getHours() <= 9 ? "0" + date.getHours().toString() : date.getHours().toString();
                    var mins = date.getMinutes() <= 9 ? "0" + date.getMinutes().toString() : date.getMinutes().toString();
                    $scope.editEvent = {
                        _id: eventData._id,
                        title: eventData.title,
                        location: eventData.location,
                        displayDate: date,
                        dateTime: date,
                        date: eventData.dateTime,
                        invite: eventData.sh_op,
                        exGroups: shGroups,
                        exFriends: shFriends,
                        sh_visib: eventData.sh_visib,
                        showDate: GlobalServices.getDateFromDateTime(date),
                        showTime: hrs + ":" + mins
                    }
                    $scope.modalEdit.show();
                });

            };

            $scope.modalStatusOpen = function () {
                $scope.modalStatus.show();
            };

            $scope.modalStatusClose = function () {
                $scope.modalStatus.hide();
            };

            $scope.editEventClose = function () {
                $scope.modalEdit.hide();
            };

            $scope.privacyEventOpen = function () {
                $scope.modalPrivacy.show();
            };

            $scope.privacyEventClose = function () {
                $scope.modalPrivacy.hide();
            };

            $scope.datePickerCallback = function (val) {
                if (typeof (val) === 'undefined') {
                    console.log('Date not selected');
                } else {
                    var time = $scope.newEvent.dateTime;
                    var date = new Date($scope.newEvent.displayDate);

                    if (val !== 'time') {
//                        var newDate = new Date(val);
                        $scope.newEvent.dateVal = val;
                        $scope.newEvent.displayDate = moment(val).format('MMM-DD-YYYY');
                        $scope.newEvent.date = moment(val).format('MMM-DD-YYYY').valueOf();
                        //$scope.newEvent.date = $scope.newEvent.date.substring(0, $scope.newEvent.date.length - 2);
                        console.log('Selected date is : ', $scope.newEvent.date);
                    } else if ($scope.newEvent.dateVal != undefined) {
                        var newDate = new Date($scope.newEvent.dateVal);

                        newDate.setHours($scope.newEvent.dateTime.getHours(), $scope.newEvent.dateTime.getMinutes(), 0, 0);
                        $scope.newEvent.displayDate = $scope.formatDate(newDate);
                        $scope.newEvent.dateTime = newDate;
                        $scope.newEvent.date = newDate.getTime().toString();
                        //$scope.newEvent.date = $scope.newEvent.date.substring(0, $scope.newEvent.date.length - 2);
                        console.log('Selected date is : ', $scope.newEvent.date);
                    }

                    date.setHours(time.getHours());
                    date.setMinutes(time.getMinutes());
                    date.setSeconds(time.getMinutes());
                    console.log(date);
                    $scope.newEvent.date = date.getTime().toString();
                }
            };

            $scope.datePickerEditCallback = function (val) {
                if (typeof (val) === 'undefined') {
                    console.log('Date not selected');
                } else {
                    if (val !== 'time') {
                        var newDate = new Date(val);
                        $scope.editEvent.dateVal = val;

                        newDate.setHours($scope.editEvent.dateTime.getHours(), $scope.editEvent.dateTime.getMinutes(), 0, 0);
                        $scope.editEvent.displayDate = $scope.formatDate(newDate);
                        $scope.editEvent.date = newDate;
                        console.log('Selected date is : ', $scope.editEvent.date);
                    } else if ($scope.editEvent.dateVal != undefined) {
                        var newDate = new Date($scope.editEvent.dateVal);

                        newDate.setHours($scope.editEvent.dateTime.getHours(), $scope.editEvent.dateTime.getMinutes(), 0, 0);
                        $scope.editEvent.displayDate = $scope.formatDate(newDate);
                        $scope.editEvent.date = newDate;
                        console.log('Selected date is : ', $scope.editEvent.date);
                    }
                }
            };

        })
        .controller('UserProfileCtrl', function ($scope, $state, GlobalServices, AuthFactory, UserProfileFactory, $ionicModal, $ionicPopup, $ionicTabsDelegate, $ionicNavBarDelegate) {

            $scope.feeds = [
                // {
                //     'name': 'ESPN',
                //     'value': 'espn',
                //     'selected': false
                // },
                // {
                //     'name': 'Google News',
                //     'value': 'googlenews',
                //     'selected': false
                // }
            ];
            $scope.newFeed = {};
            $scope.pass = {};
            $scope.pages = {};

            $scope.pages.pageNum = 0;

            $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                viewData.enableBack = true;
            });
            $ionicNavBarDelegate.showBackButton(false);

            $scope.$on('userProfileBack', function () {
                $scope.pages.pageNum = 0;
                $ionicNavBarDelegate.showBackButton(false);
                $ionicNavBarDelegate.title('Account Settings');
            });

            $scope.changeTitle = function (title) {
                $ionicNavBarDelegate.title(title);
                $ionicNavBarDelegate.showBackButton(true);
            };

            $scope.$ionicGoBack = function () {
                console.log('aaa');
            };

            $scope.loadData = function () {
                UserProfileFactory.getUserProfile().then(function (response) {
                    console.log(response);
                    $scope.up = response.data;

                    $scope.up.dob = new Date($scope.up.dob);
                    $scope.feeds = $scope.up.selectedfeeds;
                    // for (var data in $scope.up.selectedfeeds) {
                    //     for (var feed in $scope.feeds) {
                    //         if ($scope.up.selectedfeeds[data] === $scope.feeds[feed].value) {
                    //             $scope.feeds[feed].selected = true;
                    //             break;
                    //         }
                    //     }
                    // }

                });
            };


            $scope.$watch('up', function (newVal, oldVal) {
                var data = {};

                if (newVal !== undefined && oldVal !== undefined && newVal.firstname !== oldVal.firstname)
                    data.firstname = newVal.firstname;
                if (newVal !== undefined && oldVal !== undefined && newVal.lastname !== oldVal.lastname)
                    data.lastname = newVal.lastname;
                if (newVal !== undefined && oldVal !== undefined && newVal.dob !== oldVal.dob)
                    data.dob = newVal.dob;
                if (newVal !== undefined && oldVal !== undefined && newVal.selectedfeeds !== oldVal.selectedfeeds)
                    data.selectedfeeds = newVal.selectedfeeds;

                if (Object.keys(data).length > 0) {
                    UserProfileFactory.saveUserProfile(data).then(function (response) {
                        console.log(response);
                    });
                }

            }, true);

            $scope.leftButtons = [{
                    type: 'button-icon icon ion-navicon',
                    tap: function (e) {
                        $scope.toggleMenu();
                    }
                }];

            $scope.logout = function () {
                AuthFactory.logout().then(function (response) {
                    if (response.data.status == "success") {
                        GlobalServices.username = "";
                        GlobalServices.user_name = "";
                        $ionicTabsDelegate.showBar(false);
                        $state.go('login');
                    }
                });
            };

            $ionicModal.fromTemplateUrl('templates/select-feeds.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modal = modal;
            });

            $ionicModal.fromTemplateUrl('templates/change-password.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalPassword = modal;
            });

            $ionicModal.fromTemplateUrl('templates/add-news-source.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalAddNewSource = modal;
            })

            $scope.modalPassOpen = function () {
                $scope.modalPassword.show();
            };

            $scope.modalPassClose = function (key) {
                if (key === 'submit') {
                    if (!$scope.pass.newPassword || !$scope.pass.oldPassword || $scope.pass.newPassword === undefined || $scope.pass.newPassword !== $scope.pass.confirmNewPassword) {
                        $ionicPopup.alert({
                            title: 'Passwords don\'t match or empty',
                            template: 'Please enter the same passwords to new and confirm password fields'
                        });
                    } else {
                        var data = {
                            "old_password": $scope.pass.oldPassword,
                            "password": $scope.pass.newPassword
                        };

                        UserProfileFactory.saveUserProfile(data).then(function (response) {
                            console.log(response);
                            if (response.data.status === 'success') {
                                $ionicPopup.alert({
                                    title: 'Password changed',
                                    template: 'Your password changed successfully'
                                });
                                $scope.modalPassword.hide();
                            } else {
                                $ionicPopup.alert({
                                    title: 'Error Changing Password',
                                    template: 'Your old password is incorrect. Please re-enter your old password, new password and then Save again.'
                                });
                            }
                        });
                    }

                } else
                    $scope.modalPassword.hide();
            };

            $scope.modalOpen = function () {
                $scope.modal.show();
            };

            $scope.modalClose = function (key) {
                if (key === 'submit') {
                    var feeds = [];
                    for (var data in $scope.feeds) {
                        feeds.push($scope.feeds[data]);
                    }
                    $scope.up.selectedfeeds = feeds;
                }
                $scope.modal.hide();
            };

            $scope.closeNewSourceModal = function () {
                if ($scope.newFeed.srcname == "" || $scope.newFeed.srcname === undefined || $scope.newFeed.srcurl == "" || $scope.newFeed.srcurl === undefined) {
                    $ionicPopup.alert({
                        // title: '<span class="red">*</span> Fields Required',
                        template: 'Fields marked <span class="red">*</span> are required.'
                    });
                    return false;
                }
                var newFeed = {
                    'name': $scope.newFeed.srcname,
                    'url': $scope.newFeed.srcurl,
                    'selected': true
                };
                $scope.up.selectedfeeds.push(newFeed);
                $scope.modalAddNewSource.hide();
            };
        })
        .controller('InviteCtrl', function ($scope, $state) {
            $scope.leftButtons = [{
                    type: 'button-icon icon ion-navicon',
                    tap: function (e) {
                        $scope.toggleMenu();
                    }
                }];

        })
        .controller('GroupCtrl', function ($scope, $state, FriendFactory, GlobalServices, $ionicPopup) {
            $scope.leftButtons = [{
                    type: 'button-icon icon ion-navicon',
                    tap: function (e) {
                        $scope.toggleMenu();
                    }
                }];
        })
        .controller('NewsSettingsCtrl', function ($scope, $state) {
            $scope.leftButtons = [{
                    type: 'button-icon icon ion-navicon',
                    tap: function (e) {
                        $scope.toggleMenu();
                    }
                }];

        })
        .controller('MainController', function ($scope, $rootScope, $ionicTabsDelegate, usSpinnerService, $ionicNavBarDelegate) {
            $scope.toggleMenu = function () {
                $scope.sideMenuController.toggleLeft();
            };

            usSpinnerService.spin('spinner-1');

            $rootScope.userProfileBack = function () {
                $scope.$broadcast('userProfileBack');
            };

            $rootScope.pollsScreenBack = function () {
                $rootScope.pollsScreen.page = 0;
            };


            $rootScope.pollsScreen = {};

            $rootScope.pollsScreen.page = 0;

            $rootScope.interact = {};

            $scope.goToPolls = function () {
                $ionicTabsDelegate.select(5);
            };


            $rootScope.interact.page = 1;

            $rootScope.interact.secondPage = function () {
                $rootScope.interact.page = 2;
            };

            $rootScope.interact.firstPage = function () {
                $rootScope.interact.page = 1;
            };

            $scope.showTabBar = function () {
                $ionicTabsDelegate.showBar(true);
            };

            $scope.goToMain = function () {
                $rootScope.firstLogin = false;
                $ionicTabsDelegate.select(3);
                $ionicTabsDelegate.showBar(false);
            };

            $rootScope.$watch('pollsScreen.page', function (newVal, oldVal) {
                if (!newVal) {
                    $rootScope.pollsScreen.title = 'Polls';
                    $ionicNavBarDelegate.showBackButton(false);
                } else if (newVal == 1) {
                    $rootScope.pollsScreen.title = 'Create Polls';
                    $ionicNavBarDelegate.showBackButton(true);
                } else if (newVal == 2) {
                    $rootScope.pollsScreen.title = 'View Polls Results';
                    $ionicNavBarDelegate.showBackButton(true);
                } else if (newVal == 3) {
                    $rootScope.pollsScreen.title = 'Edit Polls';
                    $ionicNavBarDelegate.showBackButton(true);
                }

            });

        })
        .controller('AccountCtrl', function ($scope) {
            $scope.changePhoto = function () {
                alert('Change Profile Photo');
            }

            $scope.leftButtons = [{
                    type: 'button-icon icon ion-navicon',
                    tap: function (e) {
                        $scope.toggleMenu();
                    }
                }];
        })
        .controller('SettingsCtrl', function ($scope) {})
        .controller('PollsCtrl', function ($scope, $rootScope, $ionicModal, GlobalServices, PollFactory, Upload, $window, $timeout, FriendFactory, $state, $stateParams, $q, $ionicPopup, $cordovaImagePicker, $cordovaFileTransfer, usSpinnerService) {

            if (typeof ($stateParams["page"]) != 'undefined' && $stateParams["page"] != null) {
                $scope.pollsScreen.page = $stateParams["page"];
            } else {
                $scope.pollsScreen.page = 0;
            }

            $scope.$on('$ionicView.beforeEnter', function (event, viewData) {
                viewData.enableBack = true;
            });

            $scope.answers = {};

            $scope.polls = {};

            $scope.requestedPolls = [];

            $scope.polls.pollType = 0;

            $scope.items = [{
                    id: 1,
                    label: 'Yes/No',
                }, {
                    id: 2,
                    label: 'Star rating',
                }, {
                    id: 3,
                    label: 'Multiple answers',
                }];

            $scope.graphs = [{
                    id: 1,
                    label: "Pie Graph"
                }, {
                    id: 2,
                    label: "Bar Graph"
                }, {
                    id: 3,
                    label: "Line Graph"
                }];

            $scope.shareWith = [{
                    id: 1,
                    label: 'Everyone'
                },
                {
                    id: 2,
                    label: 'Group'
                },
                {
                    id: 3,
                    label: 'Individual'
                }];

            //$scope.selectedShare = $scope.shareWith[0];

            $scope.groups = [];

            FriendFactory.getUserGroups({"user": GlobalServices.id}).then(function (response) {
                $scope.groups = response.data;
            });



            //$scope.selectedGroup = $scope.groups[0];

            //$scope.selected = $scope.items[0];
            /** Answer Type part **/

            $scope.moreAnswers = 2;

            var dataPoll = {
                "friendID": GlobalServices.username
            };
            console.log(dataPoll);
            PollFactory.getPolls(dataPoll).then(function (response) {
                if (response.data.message === undefined) {
                    $scope.requestedPolls = response.data;
                    console.log(response);
                    getActivePolls();
                    getPollUserData();
                } else
                    $scope.requestedPolls = [];
            });


            $scope.emptyText = function () {

                if ($scope.poll.answers) {

                    if ($scope.poll.answers.answer3 == false) {
                        $scope.poll.answers.answer3 = null
                    }
                    ;
                    if ($scope.poll.answers.answer4 == false) {
                        $scope.poll.answers.answer4 = null
                    }
                    ;
                    if ($scope.poll.answers.answer3 == true) {
                        $scope.poll.answers.answer4 = null
                    }
                    ;

                }

                if ($scope.poll.newPreAnsw) {

                    if ($scope.poll.newPreAnsw.answer3 == false) {
                        $scope.poll.newPreAnsw.answer3 = null
                    }
                    ;
                    if ($scope.poll.newPreAnsw.answer4 == false) {
                        $scope.poll.newPreAnsw.answer4 = null
                    }
                    ;
                    if ($scope.poll.newPreAnsw.answer3 == true) {
                        $scope.poll.newPreAnsw.answer4 = null
                    }
                    ;

                }

            }



            /** ion-autocomplete: TypeAhead feature **/

            $scope.friendsList = function () {

                FriendFactory.getFriends({"username": GlobalServices.username}).then(function (response) {
                    if (response.data) {
                        //console.log(response.data);
                        return $scope.listF = response.data;
                    }
                });
            };

            // Get friends list
            $scope.getItems = function (query) {

                var result = [];


                for (var i = 0; i < $scope.listF.length; i++) {
                    console.log($scope.listF[i].name + '\n' + $scope.listF[i].email);
                    var name = $scope.listF[i].name;
                    var email = $scope.listF[i].email;

                    if (name.toLowerCase().search(query.toLowerCase()) !== -1) {
                        var newItem = {name: name, email: email};
                        result.push(newItem);
                    }
                }
                ;

                console.log('Return: ' + result);
                return result;

            };




            /** ion-autocomplete: TypeAhead feature  END **/

            $scope.poll = {
                "question": "",
                "selected": "",
                "selectedShare": "",
                "selectedGroup": ""
            };

            $scope.files = "";
            $scope.imgUrls = [];

            $scope.uploadFiles = function () {
                $scope.imgUrls = [];
                var options = {
                    maximumImagesCount: 2,
                    quality: 80
                };
                $cordovaImagePicker.getPictures(options)
                        .then(function (results) {
                            var str = "";
                            for (var i = 0; i < results.length; i++) {
                                console.log('Image URI: ' + results[i]);
                                str += results[i] + "<br>";
                            }
                            $scope.files = results;
                            for (var i = 0; i < results.length; i++) {
                                angular.element('#angularSpinnerId').removeClass('ng-hide');
                                $cordovaFileTransfer.upload(
                                        GlobalServices.apiurl + "uploads/uploadFiles",
                                        results[i],
                                        {"params": {"type": "polls"}}
                                ).then(function (result) {
                                    $scope.imgUrls.push(JSON.parse(result.response).imgUrls[0]);
                                    console.log($scope.imgUrls);
                                    if ($scope.imgUrls.length == results.length) {
                                        angular.element('#angularSpinnerId').addClass('ng-hide');
                                    }
                                });
                            }
                        },
                                function (error) {
                                    console.log(error);
                                }
                        );
                // if(files.length>2)
                // 	files = files.slice(0,2);
                //     $scope.files = files;
                //     if (files && files.length) {
                //     	console.log(files);
                //         Upload.upload({
                //             // url: 'http://localhost:10010/api/uploads/uploadFiles',
                //             url: GlobalServices.apiurl+"uploads/uploadFiles",
                //             arrayKey: '',
                //             data: {
                //             	type: "polls",
                //                 file: files
                //             }
                //         }).then(function (response) {
                //         	console.log(response);
                //             $timeout(function () {
                //                 $scope.result = response.data;
                //                 $scope.imgUrls = response.data.imgUrls;
                //                 console.log("finally ", response.data.imgUrls);
                //             });
                //         }, function (response) {
                //         	console.log(response);
                //             if (response.status > 0) {
                //                 $scope.errorMsg = response.status + ': ' + response.data;
                //             }
                //         }, function (evt) {
                //             $scope.progress =
                //                 Math.min(100, parseInt(100.0 * evt.loaded / evt.total));
                //         });
                //     }
            };

            $scope.removeImage = function (num) {
                $scope.files.splice(num, 1);
                $scope.imgUrls.splice(num, 1);
                //$scope.uploadFiles($scope.files);
            };

            $scope.polls.starRating = function (star) {
                $scope.polls.selectedRating = star;
            };

            $scope.savePollClicked = false;
            $scope.updatePollClicked = false;
            $scope.highlightInvalid = function (elemValid) {
                if (elemValid && $scope.savePollClicked) {
                    return elemValid;
                }
                // else if(!elemValid && !$scope.savePollClicked){
                //     return true;// create new poll and hide the error highlights
                // }
            };

            $scope.highlightInvalidEdit = function (elemValid) {
                if (elemValid && $scope.updatePollClicked) {
                    return elemValid;
                }
                // else if(!elemValid && !$scope.updatePollClicked){
                //     return false;// create new poll and hide the error highlights
                // }
            };

            $scope.savePoll = function (isFormValid) {
                $scope.savePollClicked = true;
                if (!isFormValid)
                    return false;
                if ($scope.poll.selectedShare.id === 1) {
                    var friendsEmails = [];

                    for (var i in $scope.listF) {
                        friendsEmails.push($scope.listF[i].email);
                    }

                    $scope.poll.friend = friendsEmails;
                } else if ($scope.poll.selectedShare.id === 2) {
                    /*	FriendFactory.getGroupUsers({"user": GlobalServices.id, "gid": $scope.poll.selectedGroup._id}).then(function(response){
                     var friendsEmails = [];
                     
                     for(var i in response.data){
                     friendsEmails.push(response.data[i].email);
                     }
                     
                     $scope.poll.friend = friendsEmails;
                     }); */
                }

                var data = {
                    "userID": GlobalServices.username || "",
                    "question": $scope.poll.question || "",
                    "anstype": {'id': $scope.poll.selected.id, 'label': $scope.poll.selected.label} || "",
                    "preAnswers": $scope.poll.answers || "",
                    "sharewith": {'id': $scope.poll.selectedShare.id, 'label': $scope.poll.selectedShare.label} || "",
                    "group": $scope.poll.selectedGroup || "",
                    "selfriends": $scope.poll.friend || "",
                    "imgUrls": $scope.imgUrls || [],
                    "graph": $scope.poll.selectedGraph.label,
                    "createdAt": new Date()
                }

                PollFactory.setPoll(data).then(function (response) {
                    $scope.imgUrls = [];
                    console.log(response);
                    //PollFactory.getPolls();
                    var alertPopup = $ionicPopup.alert({
                        cssClass: 'headerless text-center',
                        template: response.data.message
                    });
                    alertPopup.then(function (res) {

                        //$state.go('main.polls');
                    });
                    if (typeof (response.data.status) != 'undefined' && response.data.status == "success") {
                        $scope.poll.question = "";
                        $scope.poll.answers = "";
                        $scope.poll.selected.id = "";
                        $scope.poll.selected.label = "";
                        $scope.poll.selectedShare.id = "";
                        $scope.poll.selectedShare.label = "";
                        $scope.poll.selectedGroup = "";
                        $scope.poll.friend = "";
                        $scope.imgUrls = [];
                        $scope.poll.selectedGraph.id = "";
                        $scope.poll.selectedGraph.label = "";
                    }
                    $scope.savePollClicked = false;
                });
                $scope.pollsScreen.page = 0;
            };


            $scope.getPolls = function () {
                $scope.userID = GlobalServices.username;
                var query = {"userID": GlobalServices.username};
                var allPolls = [];

                PollFactory.getPolls(query).then(function (response) {

                    $scope.allPolls = response.data;
                    console.log(response.data);

                    for (var data in $scope.requestedPolls) {
                        for (var email in $scope.requestedPolls[data].userAnswers) {
                            if ($scope.requestedPolls[data].userAnswers[email].contactID === GlobalServices.username && !$scope.requestedPolls[data].userAnswers[email].contactAnswer.declined) {
                                $scope.allPolls.push($scope.requestedPolls[data]);
                                break;
                            }
                        }
                    }

                    for (var poll in $scope.allPolls) {
                        for (var img in $scope.allPolls[poll].imgUrls) {
                            if ($scope.allPolls[poll].imgUrls[img].substring(0, 5) == 'https') {
                                $scope.allPolls[poll].imgUrls[img] = $scope.allPolls[poll].imgUrls[img].substring(8);
                                $scope.allPolls[poll].imgUrls[img] = 'http://' + $scope.allPolls[poll].imgUrls[img];
                            } else
                                break;
                        }
                    }

                    setPollChartData();
                });


            };

            // Delete poll

            $scope.deletePoll = function (question, permanently) {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Delete',
                    template: 'Are you sure you want to delete this poll?'
                });

                confirmPopup.then(function (res) {
                    if (res) {
                        var query = {"userID": GlobalServices.username,
                            "_id": question,
                            "permanently": permanently
                        };

                        PollFactory.deletePoll(query).then(function (response) {
                            for (var i = 0, j = $scope.allPolls.length; i < j; i++) {
                                if (question == $scope.allPolls[i]._id) {
                                    $scope.allPolls.splice(i, 1);
                                    break;
                                }
                            }
                            console.log('Response: ' + response);

                        });
                    }

                });
            };



            // Find Poll that will be updated
            $scope.findPoll = function (question) {


                var query = {"userID": GlobalServices.username, "question": question};

                console.log(query.userID + '\n' + question);

                PollFactory.getPolls(query).then(function (response) {

                    $scope.currentPollData = response.data;
                    console.log(response);
                    //console.log("Find Poll result data: " + response.data.ansType[0]);

                    $scope.poll.query_question = response.data.question || "";
                    $scope.poll.newQuestion = response.data.question || "";
                    $scope.poll.newSelected = response.data.ansType[0] || "";
                    $scope.poll.newSelectedShare = response.data.shareWith[0] || "";
                    if (response.data.group)
                        $scope.poll.newSelectedGroup = response.data.group[0] || "";
                    $scope.poll.newFriend = response.data.selFriends[0] || "";
                    $scope.poll.newPreAnsw = response.data.preAnswers[0];
                    for (var i = 0, j = $scope.graphs.length; i < j; i++) {
                        if ($scope.graphs[i].label == response.data.graph) {
                            $scope.poll.newSelectedGraph = $scope.graphs[i];
                            break;
                        }
                    }

                    $scope.poll._id = response.data._id;
                    $scope.files = response.data.imgUrls;
                    $scope.imgUrls = response.data.imgUrls;

                    //if($scope.poll.newPreAnsw.answer2 != undefined) {$scope.moreAnswers = 2;}
                    //if($scope.poll.newPreAnsw.answer3 != undefined) {$scope.moreAnswers = 3;}
                    //if($scope.poll.newPreAnsw.answer3 === true) {$scope.poll.newPreAnsw.isChecked3 = true;}

                    //if($scope.poll.newPreAnsw.answer4 != undefined) {$scope.moreAnswers = 4;}
                    //if($scope.poll.newPreAnsw.answer4 === true) {$scope.poll.newPreAnsw.isChecked4 = true;}


                });


                //console.log('Result of the query: \n' + oldPollData.question);

            };


            $scope.updatePoll = function (isFormValid) {
                $scope.updatePollClicked = true;
                if (!isFormValid)
                    return false;
                console.log('updatePoll Question: ' + $scope.poll.newQuestion);
                console.log('updatePoll Answer Type: ' + $scope.poll.newSelected.label);
                console.log('updatePoll Multi Answer: ' + $scope.poll.newPreAnsw);
                console.log('updatePoll Share With: ' + $scope.poll.newSelectedShare.label + ' id:' + $scope.poll.newSelectedShare.id);
                console.log('updatePoll Selected Group: \n');
                if ($scope.poll.newSelectedGroup)
                    console.log($scope.poll.newSelectedGroup.name);
                console.log('updatePoll Contact: ' + $scope.poll.newFriend);
                console.log('updatePoll Image: ' + $scope.imgUrls);


                if ($scope.poll.newSelectedShare.id == 1) {
                    var friendsEmails = [];
                    for (var i in $scope.listF) {
                        friendsEmails.push($scope.listF[i].email);
                    }
                    console.log(friendsEmails);
                    $scope.poll.newFriend = friendsEmails;
                } else if ($scope.poll.newSelectedShare.id == 2) {
                    /*	FriendFactory.getGroupUsers({"user": GlobalServices.id, "gid": $scope.poll.newSelectedGroup._id}).then(function(response){
                     var friendsEmails = [];
                     
                     for(var i in response.data){
                     friendsEmails.push(response.data[i].email);
                     }
                     
                     $scope.poll.newFriend = friendsEmails;
                     }); */
                }

                var data = {
                    "userID": GlobalServices.username || "",
                    "_id": $scope.poll._id || "",
                    "question": $scope.poll.newQuestion || "",
                    "anstype": $scope.poll.newSelected || "",
                    "preAnswers": $scope.poll.newPreAnsw || "",
                    "sharewith": $scope.poll.newSelectedShare || "",
                    "group": $scope.poll.newSelectedGroup || "",
                    "selfriends": $scope.poll.newFriend || "",
                    "imgUrls": $scope.imgUrls || "",
                    "graph": $scope.poll.newSelectedGraph.label,
                    "modifiedAt": new Date()
                };
                PollFactory.updatePoll(data).then(function (response) {
                    console.log('Response: ' + response);
                    //PollFactory.getPolls();
                    $scope.pollsScreen.page = 2;
                    $scope.updatePollClicked = false;
                    $scope.getPolls();
                });
            };

            $scope.showMoreAnswers = function () {
                if ($scope.moreAnswers <= 3)
                    $scope.moreAnswers++;
            };

            $scope.showLessAnswers = function () {


                if ($scope.moreAnswers >= 1)
                    $scope.moreAnswers--;

                if ($scope.moreAnswers == 3) {
                    if ($scope.poll.answers) {
                        $scope.poll.answers$scope.poll.answers.answer4 = '';
                    }
                    if ($scope.poll.newPreAnsw) {
                        $scope.poll.newPreAnsw.answer4 = '';
                        $scope.poll.newPreAnsw.isChecked4 = '';
                    }
                }

                if ($scope.moreAnswers == 2) {

                    if ($scope.poll.answers) {
                        $scope.poll.answers.answer3 = '';
                        $scope.poll.answers.answer4 = '';
                    }
                    if ($scope.poll.newPreAnsw) {
                        $scope.poll.newPreAnsw.answer3 = '';
                        $scope.poll.newPreAnsw.answer4 = '';
                        $scope.poll.newPreAnsw.isChecked3 = '';
                        $scope.poll.newPreAnsw.isChecked4 = '';
                    }

                }


                if ($scope.moreAnswers == 1) {

                    if ($scope.poll.answers) {
                        $scope.poll.answers.answer2 = '';
                        $scope.poll.answers.answer3 = '';
                        $scope.poll.answers.answer4 = '';
                    }

                    if ($scope.poll.newPreAnsw) {
                        $scope.poll.newPreAnsw.answer2 = '';
                        $scope.poll.newPreAnsw.answer3 = '';
                        $scope.poll.newPreAnsw.answer4 = '';
                        $scope.poll.newPreAnsw.isChecked3 = '';
                        $scope.poll.newPreAnsw.isChecked4 = '';
                    }

                }


            };

            $ionicModal.fromTemplateUrl('templates/poll-answer.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modal = modal;
            });

            $scope.openPollModal = function (poll, type) {
                $scope.polls.currentPoll = poll;
                for (var img in $scope.polls.currentPoll.imgUrls) {
                    if ($scope.polls.currentPoll.imgUrls[img].substring(0, 5) == 'https') {
                        $scope.polls.currentPoll.imgUrls[img] = $scope.polls.currentPoll.imgUrls[img].substring(8);
                        $scope.polls.currentPoll.imgUrls[img] = 'http://' + $scope.polls.currentPoll.imgUrls[img];
                    } else
                        break;
                }
                $scope.polls.pollType = type;
                $scope.modal.show();
            };

            $scope.closePollModal = function (key) {

                if (key === 'submit') {

                    var isCustomAnsw = ($scope.polls.currentPoll.ansType[0].id === 3 && $scope.polls.selectedMultAnswer === 'custom') ? true : false;
                    var answer;

                    switch ($scope.polls.currentPoll.ansType[0].id) {
                        case 1:
                            answer = $scope.polls.selectedAnswer;
                            break;
                        case 2:
                            answer = parseInt($scope.polls.selectedRating);
                            answer = answer.toString();
                            break;
                        case 3:
                            answer = $scope.polls.selectedMultAnswer === 'custom' ? $scope.polls.customAnswer : $scope.polls.selectedMultAnswer;
                            break;
                    }

                    var data = {
                        "userID": $scope.polls.currentPoll.user_ID,
                        "_id": $scope.polls.currentPoll._id,
                        "contactID": GlobalServices.username,
                        "contactAnswer": {
                            "type": $scope.polls.currentPoll.ansType[0].id,
                            "answer": answer,
                            "custom": isCustomAnsw
                        }
                    };

                    PollFactory.savePollAnswers(data).then(function (response) {
                        console.log(response);
                        PollFactory.getPolls(dataPoll).then(function (response) {
                            if (response.data.message === undefined) {
                                $scope.requestedPolls = response.data;
                                console.log(response);
                                getActivePolls();
                                getPollUserData();
                            } else
                                $scope.requestedPolls = [];

                            $scope.modal.hide();
                        });
                    });
                } else
                    $scope.modal.hide();
            };

            $scope.declinePollRequest = function (poll) {
                var data = {
                    "userID": poll.user_ID,
                    "question": poll.question,
                    "contactID": GlobalServices.username,
                    "contactAnswer": {
                        "declined": true
                    }
                };

                PollFactory.savePollAnswers(data).then(function (response) {
                    console.log(response);
                    PollFactory.getPolls(dataPoll).then(function (responseData) {
                        if (responseData.data.message === undefined) {
                            $scope.requestedPolls = responseData.data;
                            console.log(responseData);
                            getActivePolls();
                            getPollUserData();
                        } else
                            $scope.requestedPolls = [];
                    });
                });
            };

            $scope.$on('$destroy', function () {
                $scope.modal.remove();
            });

            function getActivePolls() {
                for (var data in $scope.requestedPolls) {
                    for (var email in $scope.requestedPolls[data].userAnswers) {
                        if ($scope.requestedPolls[data].userAnswers[email].contactID === GlobalServices.username) {
                            $scope.requestedPolls[data].skipPoll = true;
                            break;
                        }
                    }
                }
            }
            ;

            function getPollUserData() {
                FriendFactory.getFriends({"username": GlobalServices.username}).then(function (response) {
                    if (response.data) {
                        var friends = response.data;

                        for (var data in $scope.requestedPolls) {
                            for (var email in friends) {
                                if ($scope.requestedPolls[data].user_ID === friends[email].email) {
                                    $scope.requestedPolls[data].name = friends[email].name;
                                    break;
                                }
                            }
                        }
                    }
                });

            }
            ;

            function setPollChartData() {
                for (var poll in $scope.allPolls) {
                    var labels = [];
                    var data = [];
                    if ($scope.allPolls[poll].ansType[0].id === 1) {
                        if ($scope.allPolls[poll].graph === "Bar Graph") {
                            labels = ['No Response', 'No', 'Yes'];
                            data = [[0, 0, 0]];
                            for (var i in $scope.allPolls[poll].userAnswers) {
                                if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "yes")
                                    data[0][2]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "no")
                                    data[0][1]++;
                                else
                                    data[0][0]++;
                            }
                        } else {
                            labels = ['No Response', 'No', 'Yes'];
                            data = [0, 0, 0];
                            for (var i in $scope.allPolls[poll].userAnswers) {
                                if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "yes")
                                    data[2]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "no")
                                    data[1]++;
                                else
                                    data[0]++;
                            }
                        }
                    } else if ($scope.allPolls[poll].ansType[0].id === 2) {
                        if ($scope.allPolls[poll].graph === "Bar Graph") {
                            labels = ['No Response', '1', '2', '3', '4', '5'];
                            data = [[0, 0, 0, 0, 0, 0]];

                            for (var i in $scope.allPolls[poll].userAnswers) {
                                if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "1")
                                    data[0][1]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "2")
                                    data[0][2]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "3")
                                    data[0][3]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "4")
                                    data[0][4]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "5")
                                    data[0][5]++;
                                else
                                    data[0][0]++;
                            }
                        } else {
                            labels = ['No Response', '1', '2', '3', '4', '5'];
                            data = [0, 0, 0, 0, 0, 0];

                            for (var i in $scope.allPolls[poll].userAnswers) {
                                if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "1")
                                    data[1]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "2")
                                    data[2]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "3")
                                    data[3]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "4")
                                    data[4]++;
                                else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === "5")
                                    data[5]++;
                                else
                                    data[0]++;
                            }
                        }
                    } else if ($scope.allPolls[poll].ansType[0].id === 3) {
                        if ($scope.allPolls[poll].graph === "Bar Graph") {
                            labels[0] = "No Response";
                            data[0] = [];
                            var key = 1;
                            for (var i in $scope.allPolls[poll].preAnswers[0]) {
                                if ($scope.allPolls[poll].preAnswers[0][i] !== null && $scope.allPolls[poll].preAnswers[0][i] !== true) {
                                    labels[key] = $scope.allPolls[poll].preAnswers[0][i];
                                    key++;
                                } else if ($scope.allPolls[poll].preAnswers[0][i] !== true) {
                                    labels[key] = "Other answer";
                                    break;
                                }
                            }

                            for (var j = 0; j < labels.length; j++) {
                                data[0][j] = 0;
                            }

                            for (var i in $scope.allPolls[poll].userAnswers) {
                                for (var j = 1; j < labels.length; j++) {
                                    if ($scope.allPolls[poll].userAnswers[i].contactAnswer.declined === true) {
                                        data[0][0]++;
                                        break;
                                    } else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.custom === true) {
                                        data[0][(labels.length - 1)]++;
                                        break;
                                    } else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === labels[j]) {
                                        data[0][j]++;
                                        break;
                                    }
                                }
                            }
                        } else {
                            labels[0] = "No Response";
                            var key = 1;
                            for (var i in $scope.allPolls[poll].preAnswers[0]) {
                                if ($scope.allPolls[poll].preAnswers[0][i] !== null && $scope.allPolls[poll].preAnswers[0][i] !== true) {
                                    labels[key] = $scope.allPolls[poll].preAnswers[0][i];
                                    key++;
                                } else if ($scope.allPolls[poll].preAnswers[0][i] !== true) {
                                    labels[key] = "Other answer";
                                    break;
                                }
                            }

                            for (var j = 0; j < labels.length; j++) {
                                data[j] = 0;
                            }

                            for (var i in $scope.allPolls[poll].userAnswers) {
                                for (var j = 1; j < labels.length; j++) {
                                    if ($scope.allPolls[poll].userAnswers[i].contactAnswer.declined === true) {
                                        data[0]++;
                                        break;
                                    } else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.custom === true) {
                                        data[(labels.length - 1)]++;
                                        break;
                                    } else if ($scope.allPolls[poll].userAnswers[i].contactAnswer.answer === labels[j]) {
                                        data[j]++;
                                        break;
                                    }
                                }
                            }
                        }
                    }

                    $scope.allPolls[poll].labels = labels;
                    if ($scope.allPolls[poll].graph === "Line Graph") {
                        var lineData = [];
                        lineData[0] = data;
                        $scope.allPolls[poll].data = lineData;
                    } else
                        $scope.allPolls[poll].data = data;
                }
            }
            ;

        })
        .controller('InteractCtrl', function ($scope, $state, $ionicModal, $rootScope, PostFactory, emojiService, PrivateMessages, usSpinnerService, $ionicPopup, $timeout, $interval, FriendFactory, GlobalServices, $ionicScrollDelegate, $ionicPopover, $cordovaImagePicker, $cordovaFileTransfer) {
            $('body').removeClass('platform-android');
            $('body').removeClass('platform-ios');
            document.body.classList.add('platform-ionic');

            FriendFactory.getFriends().then(function (response) {
                if (response.data) {
                    $scope.listFriends = response.data;
                    FriendFactory.getUserGroups().then(function (response) {
                        if (response.data)
                            $scope.listGroups = response.data;
                        console.log($scope.listGroups);
                    });
                }
            });

            $scope.userEmail = GlobalServices.username;

            $scope.getItems = function (query) {

                var result = [];


                for (var i = 0; i < $scope.listFriends.length; i++) {
                    console.log($scope.listFriends[i].name + '\n' + $scope.listFriends[i].email);
                    var name = $scope.listFriends[i].name;
                    var id = $scope.listFriends[i]._id;

                    if (name.toLowerCase().search(query.toLowerCase()) !== -1) {
                        var newItem = {name: name, _id: id};
                        result.push(newItem);
                    }
                }
                ;

                console.log('Return: ' + result);
                return result;

            };

            $scope.getGroupItems = function (query) {

                var result = [];

                for (var i = 0; i < $scope.listGroups.length; i++) {
                    console.log($scope.listGroups[i].name + '\n' + $scope.listGroups[i]._id);
                    var name = $scope.listGroups[i].name;
                    var id = $scope.listGroups[i]._id;

                    if (name.toLowerCase().search(query.toLowerCase()) !== -1) {
                        var newItem = {name: name, _id: id};
                        result.push(newItem);
                    }
                }
                ;

                console.log('Return: ' + result);
                return result;

            };

            $scope.emojiListPeople = emojiService.emojiListPeople;
            $scope.emojiListNature = emojiService.emojiListNature;
            $scope.emojiListObjects = emojiService.emojiListObjects;
            $scope.emojiListPlaces = emojiService.emojiListPlaces;
            $scope.emojiListSymbols = emojiService.emojiListSymbols;
            $scope.emojis = emojiService.emojis;
            $scope.input = {
                message: ''
            };
            $scope.sort = {
                friendSort: '',
                chatSort: ''
            }
            var promise;
            var userDetails = localStorage.getItem('userDetails');

            $scope.posts = {};

            $scope.posts.shareWith = [
                {
                    id: 1,
                    label: 'All friends'
                },
                {
                    id: 2,
                    label: 'Groups'
                },
                {
                    id: 3,
                    label: 'Individual'
                }
            ];

            $scope.posts.selectedShare = $scope.posts.shareWith[0];

            console.log(userDetails);
            $scope.myId = JSON.parse(userDetails)._id;
            console.log('MyId', $scope.myId);
            var viewScroll = $ionicScrollDelegate.$getByHandle('userMessageScroll');
            $scope.showAlert2 = function () {
                var hj = $scope.shy.status;
                var alertPopup = $ionicPopup.alert({
                    title: hj

                });
                alertPopup.then(function (res) {
                    console.log('Thank you for not eating my delicious ice cream cone');
                });
            };
            $scope.showAlert1 = function () {
                var hjk = $scope.shos.status;
                var alertPopup = $ionicPopup.alert({
                    title: hjk

                });
                alertPopup.then(function (res) {
                    console.log('Thank you for not eating my delicious ice cream cone');
                });
            };

            $ionicModal.fromTemplateUrl('templates/fullchat.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modal = modal;
            });

            $ionicModal.fromTemplateUrl('templates/friendListChat.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modal_friends = modal;
            });

            $ionicModal.fromTemplateUrl('templates/chatImageFullscreen.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modal_chat_image = modal;
            });

            $ionicModal.fromTemplateUrl('templates/edit-post.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.editPostModal = modal;
            });

            $ionicModal.fromTemplateUrl('templates/share-post.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.sharePostModal = modal;
            });

            $rootScope.showBoxOne = false;
            $rootScope.comm = "";
            var data = {
                "user": "67430312"
            };

            $scope.$watch('interact.page', function (newVar, oldVar) {
                if (newVar === 1)
                    $scope.posts.getPosts();
                else if (newVar === 2) {
                    var data = {
                        'page': 1
                    };

                    PrivateMessages.getInitiatedChats({}).then(function (response) {
                        if (response.data) {
                            console.log(response.data);
                            $scope.initiatedChatList = response.data.data;
                        }
                    });
                }
            });

            $scope.likeget = function () {



                var data = {
                    "content": "67430312"


                };
                PostFactory.likelist(data).then(function (response) {
                    console.log(response);
                    $rootScope.lik = response.data;

                });
            };

            $scope.enterr = function (accept) {
                var data = {
                    "comment": "Some text",
                    "post_id": "97407148",
                    "user_id": "23800258"
                };
                PostFactory.postcomment(data).then(function (response) {

                    console.log(response);
                    $scope.shos = response.data;
                    $scope.showAlert1();



                });
            };
            $scope.likke = function () {

                var data = {
                    "post_id": "97407148",
                    "user_id": "23800258"
                };
                PostFactory.like(data).then(function (response) {
                    console.log(response);
                    $scope.shy = response.data;
                    $scope.showAlert2();


                });
            };

            $scope.commentt = function (posts) {


                $rootScope.singleval = posts;


                $state.go('main.comment');

            };

            $scope.open1 = function () {

                $state.go('main.post');

            };

            $scope.openFriendList = function () {
                FriendFactory.getFriends({"username": GlobalServices.username}).then(function (response) {
                    if (response.data) {
                        console.log(response.data);
                        $scope.friendslist = response.data;
                    }
                });
                $scope.sort.friendSort = '';
                $scope.modal_friends.show();
            }

            $scope.closeFriendsPopup = function () {
                $scope.modal_friends.hide();
            }

            $scope.startChat = function (friend) {
                console.log(friend);
                console.log('add event open');
                $scope.page = 0;
                $scope.canFetchMessage = true;
                $scope.chatMessages = [];
                $scope.currentFriend = friend;
                $scope.currentFriend.friend_id = friend._id;
                var postData = {
                    page: $scope.page,
                    friend_id: friend._id,
                }
                startChatPolling(postData);
                getMessages(postData);
            }

            $scope.addEventOpen = function (friend) {
                console.log('add event open');
                $scope.page = 0;
                $scope.canFetchMessage = true;
                $scope.chatMessages = [];
                $scope.currentFriend = friend;
                $scope.friend_id;
                if (friend.toUser.length > 0) {
                    $scope.friend_id = friend.toUser[0]._id;
                    $scope.currentFriend.name = friend.toUser[0].name;
                } else {
                    $scope.friend_id = friend.fromUser[0]._id;
                    $scope.currentFriend.name = friend.fromUser[0].name;
                }
                $scope.currentFriend.friend_id = $scope.friend_id;
                var postData = {
                    page: $scope.page,
                    friend_id: $scope.friend_id,
                }
                startChatPolling(postData);
                getMessages(postData);
            };

            $scope.getName = function (friend) {
                var name = '';
                if (friend.toUser.length > 0) {
                    name = friend.toUser[0].name;
                } else {
                    name = friend.fromUser[0].name;
                }
                return name;
            }

            function getMessages(postData) {
                usSpinnerService.spin('spinner-1');
                PrivateMessages.getMessages(postData).then(function (response) {
                    console.log(response);
                    if (response.status == 200) {
                        $scope.page = $scope.page + 1;
                        var chatData = response.data;
                        if (chatData.length == 0) {
                            $scope.canFetchMessage = false;
                        }
                        for (var i = 0, j = chatData.length; i < j; i++) {
                            if (!checkIfDuplicate(chatData[i], $scope.chatMessages)) {
                                $scope.chatMessages.push(chatData[i]);
                            } else {
                                console.log('Duplicate');
                            }
                        }
                        if ($scope.chatMessages.length > 0) {
                            if ($scope.chatMessages[0].sent_time !== undefined) {
                                $scope.latestMsg = $scope.chatMessages[0];
                            }
                        }
                        $scope.closeFriendsPopup();
                        $scope.modal.show();

                        handlePageScroll(postData.page);

                        if (postData.page == 0) {
                            $timeout(function () {
                                viewScroll.scrollBottom();
                            }, 0);
                        }

                    } else {
                        $ionicPopup.alert({
                            title: "Error",
                            template: "Error occured while making request"
                        });
                    }
                });
            }

            function handlePageScroll(page) {
                $timeout(function () {
                    //$scope.current_scroll_height = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollView().__maxScrollTop;
                    $scope.current_scroll_height = document.getElementById('chatWindow').scrollHeight;
                    if (page > 0) {
                        console.log($scope.current_scroll_height - $scope.last_scroll_height);
                        viewScroll.scrollTo(0, $scope.current_scroll_height - $scope.last_scroll_height);
                    }
                    $scope.last_scroll_height = angular.copy($scope.current_scroll_height);
                    $scope.scrolledDownOnce = true;
                    console.log($scope.last_scroll_height);
                }, 50);
            }

            function startChatPolling() {
                var postData = {
                    page: 0,
                    friend_id: $scope.friend_id,
                }
                $scope.chatTimout = $timeout(function () {
                    $scope.promise = $interval(function () {
                        getLatestMessages(postData);
                    }, 10000);
                }, 10000);
            }

            function getLatestMessages(postData) {
                usSpinnerService.stop('spinner-1');
                PrivateMessages.getMessages(postData).then(function (response) {
                    console.log(response);
                    if (response.status == 200) {
                        $scope.page = $scope.page + 1;
                        var chatData = response.data;
                        for (var i = 0, j = chatData.length; i < j; i++) {
                            if ($scope.latestMsg.sent_time < chatData[i].sent_time && chatData[i].from._id !== $scope.myId) {
                                $scope.chatMessages.unshift(chatData[i]);
                                viewScroll.scrollBottom();
                            }
                        }
                        if ($scope.chatMessages.length > 0) {
                            if ($scope.chatMessages[0].sent_time !== undefined) {
                                $scope.latestMsg = $scope.chatMessages[0];
                            }
                        }
                        console.log($scope.latestMsg._id);
                        handlePageScroll(postData.page);
                    }
                });
            }

            $scope.addEventClose = function () {
                console.log('add event close');
                usSpinnerService.spin('spinner-1');
                $scope.currentFriend = {};
                $timeout.cancel($scope.chatTimout);
                $interval.cancel($scope.promise);
                $scope.modal.hide();
            };

            $scope.sendMessage = function (message, type, imgUrl) {
                console.log(message);
                $scope.chatMessages.unshift({msg: message, type: type, img_path: imgUrl, from: {_id: $scope.myId}});
                $timeout(function () {
                    viewScroll.scrollBottom(true);
                }, 0);
                $scope.input.message = '';
                usSpinnerService.stop('spinner-1');

                var postData = {
                    type: type,
                    to: $scope.currentFriend.friend_id,
                    msg: message,
                    img_path: imgUrl,
                }
                console.log("image check", imgUrl);
                PrivateMessages.sendMessage(postData).then(function (response) {
                    console.log(response);
                    if (response.status == 200) {
                        $interval.cancel($scope.promise);
                        startChatPolling();
                        console.log('sent');
                    } else {
                        $ionicPopup.alert({
                            title: "Error",
                            template: "Error occured while sending message"
                        });
                    }
                });
            }

            $scope.uploadFile = function () {
                var options = {
                    maximumImagesCount: 1,
                    quality: 80
                };
                $cordovaImagePicker.getPictures(options)
                        .then(function (results) {
                            console.log(results);
                            for (var i = 0; i < results.length; i++) {
                                // $ionicPopup.alert({
                                //     title: "debug",
                                //     template: results[i]
                                // });
                                $cordovaFileTransfer.upload(
                                        GlobalServices.apiurl + "uploads/uploadFile",
                                        results[i],
                                        {"params": {"type": "pmessage"}}
                                ).then(function (result) {
                                    console.log(result);
                                    console.log('imgPath', JSON.parse(result.response).imgUrls);
                                    $scope.sendMessage('', 'Image', JSON.parse(result.response).imgUrls)
                                });
                            }
                        },
                                function (error) {
                                    console.log(error);
                                }
                        );
            };

            $scope.posts.imgUrls = [];
            $scope.posts.commentTexts = [];

            $scope.posts.uploadFile = function () {
                var options = {
                    maximumImagesCount: 1,
                    quality: 80
                };
                $cordovaImagePicker.getPictures(options)
                        .then(function (results) {
                            console.log(results);
                            for (var i = 0; i < results.length; i++) {
                                $ionicPopup.alert({
                                    title: "debug",
                                    template: results[i]
                                });
                                $cordovaFileTransfer.upload(
                                        GlobalServices.apiurl + "uploads/uploadFile",
                                        results[i],
                                        {"params": {"type": "pmessage"}}
                                ).then(function (result) {
                                    console.log(result);
                                    console.log('imgPath', JSON.parse(result.response).imgUrls);
                                    $scope.posts.imgUrls = [];
                                    $scope.posts.imgUrls.push(JSON.parse(result.response).imgUrls);
                                });
                            }
                        },
                                function (error) {
                                    console.log(error);
                                }
                        );
            };

            $scope.getScrollPos = function () {
                if ($ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition().top == 0) {
                    console.log('reached top');
                    if ($scope.canFetchMessage == true && $scope.scrolledDownOnce == true) {
                        var postData = {
                            page: $scope.page,
                            friend_id: $scope.friend_id,
                        }
                        getMessages(postData);
                    }
                } else if ($ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollPosition().top >= $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollView().__maxScrollTop) {
                    console.log('Scrolled to end');
                    //clearOlderMessage();
                }

            }

            function checkIfDuplicate(obj, list) {
                var i;
                for (i = 0; i < list.length; i++) {

                    if (list[i]._id === obj._id) {
                        console.log("compare: " + obj._id + ' | ' + list[i]._id);
                        return true;
                    }
                }

                return false;
            }

            function clearOlderMessage() {
                if ($scope.chatMessages.length > 10) {
                    console.log('yes delete');
                    $scope.chatMessages.splice(10, $scope.chatMessages.length);
                    console.log($scope.chatMessages.length);
                    $scope.page = 1;
                    $scope.canFetchMessage = true;
                    $scope.$apply();
                    $timeout(function () {
                        //$scope.current_scroll_height = $ionicScrollDelegate.$getByHandle('userMessageScroll').getScrollView().__maxScrollTop;
                        //$scope.last_scroll_height = angular.copy($scope.current_scroll_height);
                        $scope.current_scroll_height = document.getElementById('chatWindow').scrollHeight;
                        $scope.last_scroll_height = angular.copy($scope.current_scroll_height);
                        console.log("scroll end", $scope.current_scroll_height);
                    }, 500);
                }
            }

            $ionicPopover.fromTemplateUrl('templates/emoji_popover.html', {
                scope: $scope,
            }).then(function (popover) {
                $scope.emoji_popover = popover;
            });

            $scope.getEmoji = function (emoji) {
                console.log(emoji);
                $scope.input.message = $scope.input.message + ' :' + emoji + ':';
            }

            $scope.showChatImage = function (image) {
                $scope.chatViewImage = image;
                $scope.modal_chat_image.show();
            }

            $scope.hideChatImage = function () {
                $scope.modal_chat_image.hide();
            }

            $scope.posts.addPost = function () {

                var selFriends = [], shareWith = [];

                shareWith[0] = {};

                if ($scope.posts.selectedShare.id == 1) {
                    for (var i in $scope.listFriends) {
                        selFriends.push($scope.listFriends[i]._id);
                    }
                    shareWith[0].label = 'All friends';
                    shareWith[0].id = 1;
                    selFriends.push(GlobalServices.id);
                } else if ($scope.posts.selectedShare.id == 2) {
                    console.log($scope.posts.selectedGroups);

                    shareWith[0].label = 'Groups';
                    shareWith[0].id = 2;
                    shareWith[0].groupIds = $scope.posts.selectedGroups;

                    for (var i in $scope.listGroups) {
                        for (var j in $scope.posts.selectedGroups) {
                            if ($scope.listGroups[i]._id == $scope.posts.selectedGroups[j]) {
                                for (var k in $scope.listGroups[i].users) {
                                    selFriends.push($scope.listGroups[i].users[k].id);
                                }
                            }
                        }
                    }

                    selFriends.push(GlobalServices.id);
                } else if ($scope.posts.selectedShare.id == 3) {

                    var friends = [];

                    for (var i in $scope.posts.selectedFriends) {
                        friends.push($scope.posts.selectedFriends[i]);
                    }

                    selFriends = $scope.posts.selectedFriends;
                    selFriends.push(GlobalServices.id);

                    console.log(selFriends, $scope.posts.selectedFriends, friends);

                    shareWith[0].label = 'Individual';
                    shareWith[0].id = 3;
                    shareWith[0].friendIds = friends;
                }

                data = {
                    private: true,
                    user: {
                        id: GlobalServices.id.toString(),
                        email: GlobalServices.username,
                        fullName: GlobalServices.user_name
                    },
                    content: {
                        postBody: $scope.posts.postText
                    },
                    selFriends: selFriends,
                    shareWith: shareWith
                };

                if ($scope.posts.imgUrls.length)
                    data.content.postImagesUrls = $scope.posts.imgUrls;
                if ($scope.posts.postText)
                    PostFactory.addPosts(data).then(function (response) {
                        console.log(response);
                        $scope.posts.getPosts();
                    });

            };

            $scope.posts.getPosts = function () {

                PostFactory.getPosts({bySelFriend: GlobalServices.id}).then(function (response) {

                    console.log(response);
                    if (response.data.status !== 'failed') {
                        $scope.posts.postsData = response.data;

                        for (var post in $scope.posts.postsData) {
                            if ($scope.posts.postsData[post].sharedBy.length) {
                                for (var user in $scope.posts.postsData[post].sharedBy) {

                                }
                            }
                        }

                    } else
                        $scope.posts.postsData = [];

                });

            };

            $scope.posts.deletePost = function (index) {
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Delete post',
                    template: 'Are you sure you want to delete this post?'
                });

                confirmPopup.then(function (res) {
                    if (res) {

                        var data = {
                            byPostId: $scope.posts.postsData[index]._id,
                            user: {
                                id: GlobalServices.id
                            },
                            edit: true,
                            delPost: true
                        };

                        PostFactory.addPosts(data).then(function (response) {
                            console.log(response);
                            $scope.posts.getPosts();
                        });
                    } else {

                    }
                });
            };

            $scope.posts.openEditPost = function (index) {

                $scope.posts.currentPost = $scope.posts.postsData[index];

                $scope.editPostModal.show();
            };

            $scope.posts.closeEditPost = function (edit) {
                if (edit === 'edit') {

                    var selFriends = [], shareWith = [];

                    shareWith[0] = {};

                    if ($scope.posts.currentPost.shareWith[0].id == 1) {
                        for (var i in $scope.listFriends) {
                            selFriends.push($scope.listFriends[i]._id);
                        }
                        shareWith[0].label = 'All friends';
                        shareWith[0].id = 1;
                        selFriends.push(GlobalServices.id);
                    } else if ($scope.posts.currentPost.shareWith[0].id == 2) {

                        shareWith[0].label = 'Groups';
                        shareWith[0].id = 2;
                        shareWith[0].groupIds = $scope.posts.selectedGroups;

                        for (var i in $scope.listGroups) {
                            for (var j in $scope.posts.selectedGroups) {
                                if ($scope.listGroups[i]._id == $scope.posts.selectedGroups[j]) {
                                    for (var k in $scope.listGroups[i].users) {
                                        selFriends.push($scope.listGroups[i].users[k].id);
                                    }
                                }
                            }
                        }

                        selFriends.push(GlobalServices.id);
                    } else if ($scope.posts.currentPost.shareWith[0].id == 3) {

                        var friends = [];

                        for (var i in $scope.posts.selectedFriends) {
                            friends.push($scope.posts.selectedFriends[i]);
                        }

                        selFriends = $scope.posts.selectedFriends;
                        selFriends.push(GlobalServices.id);

                        shareWith[0].label = 'Individual';
                        shareWith[0].id = 3;
                        shareWith[0].friendIds = friends;
                    }

                    data = {
                        byPostId: $scope.posts.currentPost._id,
                        private: true,
                        user: {
                            id: GlobalServices.id.toString(),
                            email: GlobalServices.username,
                            fullName: GlobalServices.user_name
                        },
                        content: {
                            postBody: $scope.posts.currentPost.content.postBody
                        },
                        selFriends: selFriends,
                        shareWith: shareWith,
                        comments: $scope.posts.currentPost.comments,
                        edit: true
                    };
                    if ($scope.posts.currentPost.content.postBody) {
                        PostFactory.addPosts(data).then(function (response) {
                            console.log(response);
                            $scope.posts.getPosts();
                        });
                    }

                    $scope.editPostModal.hide();
                } else
                    $scope.editPostModal.hide();
            };

            $scope.posts.openSharePost = function (index) {

                $scope.posts.currentPost = $scope.posts.postsData[index];

                $scope.posts.sharePostWith = {};
                $scope.sharePostModal.show();
            };

            $scope.posts.closeSharePost = function (share) {
                if (share === 'share') {

                    var selFriends = [];

                    if ($scope.posts.sharePostWith.id == 1) {
                        for (var i in $scope.listFriends) {
                            selFriends.push($scope.listFriends[i]._id);
                        }
                        selFriends.push(GlobalServices.id);
                    } else if ($scope.posts.sharePostWith.id == 2) {

                        for (var i in $scope.listGroups) {
                            for (var j in $scope.posts.sharePostWith.groupIds) {
                                if ($scope.listGroups[i]._id == $scope.posts.sharePostWith.groupIds[j]) {
                                    for (var k in $scope.listGroups[i].users) {
                                        selFriends.push($scope.listGroups[i].users[k].id);
                                    }
                                }
                            }
                        }

                        selFriends.push(GlobalServices.id);
                    } else if ($scope.posts.sharePostWith.id == 3) {

                        var friends = [];

                        for (var i in $scope.posts.sharePostWith.friendIds) {
                            friends.push($scope.posts.sharePostWith.friendIds[i]);
                        }

                        selFriends = $scope.posts.sharePostWith.friendIds;
                        selFriends.push(GlobalServices.id);
                    }


                    PostFactory.sharePosts({
                        userId: $scope.posts.currentPost.user.id,
                        byPostId: $scope.posts.currentPost._id,
                        sharedBy: {
                            id: GlobalServices.id.toString(),
                            fullName: GlobalServices.user_name,
                            selFriends: selFriends
                        }
                    }).then(function (response) {

                        console.log(response);

                        $scope.sharePostModal.hide();
                        $scope.posts.getPosts();
                    });
                } else
                    $scope.sharePostModal.hide();
            };

            $scope.posts.addComment = function (index) {
                $scope.posts.currentPost = $scope.posts.postsData[index];

                var now = new Date();

                var comments = {
                    commentBody: $scope.posts.commentTexts[index],
                    id: GlobalServices.id.toString(),
                    created_at: now.getTime().toString()
                };

                data = {
                    byPostId: $scope.posts.currentPost._id,
                    user: {
                        id: $scope.posts.currentPost.user.id
                    },
                    comment: comments,
                    edit: true
                };

                if ($scope.posts.commentTexts[index]) {

                    PostFactory.addPosts(data).then(function (response) {
                        console.log(response);
                        $scope.posts.getPosts();
                    });
                }
            };

            $scope.posts.delComment = function (userID, postID, commentID) {
                PostFactory.delComment({
                    userId: userID,
                    byPostId: postID,
                    byCommentId: commentID
                }).then(function (response) {
                    $scope.posts.getPosts();
                });
            };

            $scope.posts.addLike = function (index) {
                $scope.posts.currentPost = $scope.posts.postsData[index];

                data = {
                    byPostId: $scope.posts.currentPost._id,
                    user: {
                        id: $scope.posts.currentPost.user.id
                    },
                    content: {
                        postBody: $scope.posts.currentPost.content.postBody
                    },
                    likedById: GlobalServices.id.toString(),
                    edit: true
                };

                PostFactory.addPosts(data).then(function (response) {
                    console.log(response);
                    $scope.posts.getPosts();
                });
            };


            $scope.modelToItemMethodGroup = function (modelValue) {

                for (var i = 0; i < $scope.listGroups.length; i++) {
                    if (modelValue == $scope.listGroups[i]._id) {
                        var name = $scope.listGroups[i].name;
                        var id = $scope.listGroups[i]._id;

                        return {name: name, _id: id};
                    }
                }
            };

            $scope.modelToItemMethodFriend = function (modelValue) {

                for (var i = 0; i < $scope.listFriends.length; i++) {
                    if (modelValue == $scope.listFriends[i]._id) {
                        var name = $scope.listFriends[i].name;
                        var id = $scope.listFriends[i]._id;

                        return {name: name, _id: id};

                    }
                }

            };

        })
        .controller('CommentCtrl', function ($scope, $state, $ionicModal, $rootScope, PostFactory) {

            var data = {
                "content": "67430312"



            };
            PostFactory.likelist(data).then(function (response) {
                console.log(response);

                $rootScope.likey = response.data;

            });
            $ionicModal.fromTemplateUrl('templates/modal.html', {
                scope: $scope
            }).then(function (modal) {
                $scope.modal = modal;

            });

            $scope.createContact = function () {

                $scope.modal.hide();
            };

            var data = {
                "content": "67430312"



            };
            PostFactory.getcomment(data).then(function (response) {
                console.log(response);

                $rootScope.getco = response.data;

            });
            $rootScope.commm = "";
            $scope.enterrr = function (accept) {



                var data = {
                    "comment": "Some text",
                    "post_id": "97407148",
                    "user_id": "23800258"

                };
                PostFactory.postcomment(data).then(function (response) {
                    console.log(response);


                });
            };



        })
        .controller('PostCtrl', function ($scope, $state, $ionicModal, $http, $window, $rootScope, PostFactory, $ionicPopup, $timeout) {
            $scope.showAlert = function () {
                var poss;
                if ($rootScope.geo != null) {
                    poss = $rootScope.geo.status;
                } else {
                    poss = "error";
                }
                var alertPopup = $ionicPopup.alert({
                    title: poss,
                    template: '<center>Success fully posted!</center>'
                });
                alertPopup.then(function (res) {
                    console.log('Thank you for not eating my delicious ice cream cone');
                });
            };

            $scope.file = "";
            $scope.textt = "";
            $scope.onChange = function (e, fileList) {


            };

            $scope.onLoad = function (e, reader, file, fileList, fileOjects, fileObj) {

            };

            var uploadedCount = 0;

            $scope.files = [];

            $scope.postt = function (postc, filec) {




                console.log(filec);
                var vidd = "";
                var imgg = "";
                filec.filetype.substring(0, 4)
                if (filec.filetype.substring(0, 5) == "video") {
                    vidd = filec.base64;

                    var data = {
                        "content": "67430312",
                        "edit": "false",
                        "video": vidd,
                        "tags": ["78308149", "25937201"]

                    };

                    PostFactory.apost(data).then(function (response) {
                        console.log(response);



                        $rootScope.geo = response.data;

                        $scope.showAlert();
                    });



                }

                if (filec.filetype.substring(0, 5) == "image") {
                    imgg = filec.base64;

                    var data = {
                        "content": "67430312",
                        "edit": "false",
                        "images": imgg,
                        "tags": ["78308149", "25937201"]

                    };

                    PostFactory.apost(data).then(function (response) {
                        console.log(response);



                        $rootScope.geo = response.data;

                        $scope.showAlert();
                    });

                }



            };



        })
        .controller('FriendsCtrl', function ($scope, $state, $ionicModal, $q, $ionicPopover, $ionicPopup, FriendFactory, GlobalServices) {
            $scope.searchFriend = "";
            $scope.friend = {
                "name": "",
                "email": ""
            };

            $scope.friends = {};
            $scope.connection = {};
            $scope.connection.page = 1;
            $scope.groups = {};
            $scope.goToConn = function () {
                $state.go('main.connection');
            };
            $scope.groups.groupTypes = [{
                    id: 1,
                    label: 'Friends'
                }, {
                    id: 2,
                    label: 'Family'
                }, {
                    id: 3,
                    label: 'Colleagues'
                }, {
                    id: 4,
                    label: 'Custom'
                }];
            $scope.friend.mobNumberValid = false;
            $scope.friend.validatePhoneNum = function (key) {
                if (key == 1) {
                    if ($scope.friend.mobNumberFirst.length == 3) {
                        var element = document.getElementById('mob_num_input_second');
                        element.focus();
                    } else if ($scope.friend.mobNumberFirst.length > 3) {
                        $scope.friend.mobNumberFirst = $scope.friend.mobNumberFirst.substring(0, 3);
                        var element = document.getElementById('mob_num_input_second');
                        element.focus();
                    }
                } else if (key == 2) {
                    if ($scope.friend.mobNumberSecond.length == 3) {
                        var element = document.getElementById('mob_num_input_third');
                        element.focus();
                    } else if ($scope.friend.mobNumberSecond.length > 3) {
                        $scope.friend.mobNumberSecond = $scope.friend.mobNumberSecond.substring(0, 3);
                        var element = document.getElementById('mob_num_input_third');
                        element.focus();
                    }
                } else if (key == 3) {
                    if ($scope.friend.mobNumberThird.length > 4) {
                        $scope.friend.mobNumberThird = $scope.friend.mobNumberThird.substring(0, 4);
                    }
                }

                $scope.friend.mobNumber = $scope.friend.mobNumberFirst + $scope.friend.mobNumberSecond + $scope.friend.mobNumberThird;

                if ($scope.friend.mobNumber == 10) {
                    $scope.friend.mobNumberValid = true;
                } else
                    $scope.friend.mobNumberValid = false;

            };

            $scope.friends.acceptConnection = function (id) {
                var friendId = id.toString();
                FriendFactory.acceptConnection({"friend_id": friendId}).then(function (response) {
                    console.log(response);
                    FriendFactory.getFriends({"username": GlobalServices.username}).then(function (response) {
                        if (response.data) {
                            console.log(response.data);
                            $scope.friendslist = response.data;
                        }
                    });
                });
            };

            $scope.friends.skipConnection = function (id) {
                var friendId = id.toString();
                FriendFactory.skipConnection({"friend_id": friendId}).then(function (response) {
                    console.log(response);
                    FriendFactory.getFriends({"username": GlobalServices.username}).then(function (response) {
                        if (response.data) {
                            console.log(response.data);
                            $scope.friendslist = response.data;
                        }
                    });
                });
            };

            $scope.friends.removeConnection = function (id) {
                var friendId = id.toString();
                var confirmPopup = $ionicPopup.confirm({
                    title: 'Remove Friend',
                    template: 'Are you sure you want to remove your friend?'
                });

                confirmPopup.then(function (res) {
                    if (res) {

                        FriendFactory.removeConnection({"friend_id": friendId}).then(function (response) {
                            console.log(response);
                            FriendFactory.getFriends({"username": GlobalServices.username}).then(function (response) {
                                if (response.data) {
                                    console.log(response.data);
                                    $scope.friendslist = response.data;
                                }
                            });
                        });
                    } else {

                    }
                });

            };

            $scope.groups.selectedGroupType = $scope.groups.groupTypes[0];
            var initGroupList = function () {
                FriendFactory.getUserGroups({"user": GlobalServices.id}).then(function (response) {
                    $scope.groups.grouplist = response.data;
                    console.log(response.data)
                });
            };
            initGroupList();
            $scope.groups.selectedGroupFriends = {};
            var addedGroupdFriends = [];

            $scope.groups.addGroup = function (isEdit) {
                // if ($scope.groups.selectedGroupType.id != 4 || $scope.groups.selectedGroupType.id == 4 && $scope.groups.customTypeName) {

                if (isEdit == 'edit') {
                    var groupType;

                    // if ($scope.groups.selectedGroupType.id == 4 && $scope.groups.customTypeName)
                    //     groupType = $scope.groups.customTypeName;
                    // else
                    //     groupType = $scope.groups.selectedGroupType.label;

                    var data = {
                        "name": $scope.groups.groupname,
                        // "type": groupType,
                        "grpid": $scope.groups.currentGroup._id,
                        // "owner": GlobalServices.id,
                        "users": $scope.groups.currentGroup.users,
                        "edit": true

                    };

                    FriendFactory.addGroup(data).then(function (response) {
                        console.log(response);
                        $scope.addGroupClose();
                        $scope.getGroupUsersClose();
                        // $state.reload();
                        $scope.connection.page = 2;
                        $scope.groups.groupname = "";
                        initGroupList();
                    });
                } else {

                    for (var props in $scope.groups.selectedGroupFriends) {
                        if ($scope.groups.selectedGroupFriends[props] === true) {
                            for (var friend in $scope.friendslist) {
                                if ($scope.friendslist[friend]._id === props)
                                    addedGroupdFriends.push({
                                        id: props,
                                        name: $scope.friendslist[friend].name,
                                        email: $scope.friendslist[friend].email
                                    });
                            }

                        }
                    }

                    // var groupType;

                    // if ($scope.groups.selectedGroupType.id == 4 && $scope.groups.customTypeName)
                    //     groupType = $scope.groups.customTypeName;
                    // else
                    //     groupType = $scope.groups.selectedGroupType.label;

                    var data = {
                        "name": $scope.groups.groupname,
                        // "type": groupType,
                        // "owner": GlobalServices.id,
                        "users": addedGroupdFriends
                    };

                    FriendFactory.addGroup(data).then(function (response) {
                        console.log(response);
                        // $scope.groups = {};
                        $scope.addGroupClose();
                        $scope.getGroupUsersClose();
                        $scope.groups.groupname = "";
                        $scope.connection.page = 2;
                        initGroupList();
                    });
                }
                // }

                $scope.groups.selectedGroupFriends = {};
            };

            $scope.groups.removeCurrentGroup = function (gid) {

                var confirmPopup = $ionicPopup.confirm({
                    title: 'Delete ' + $scope.groups.currentGroup.name + ' Group',
                    template: 'Are you sure you want to delete this group?'
                });

                confirmPopup.then(function (res) {
                    if (res) {
                        FriendFactory.removeGroup({"gid": gid}).then(function (response) {
                            $scope.getGroupUsersClose();
                            initGroupList();
                        });
                    }
                });

            };

            $scope.groups.removeUsersCurrentGroup = function (group) {


            };

            function checkIfUserExist(userData) {
                return $q(function (resolve, reject) {
                    // var data = {email: userData.email, mobile: userData.mobile};
                    FriendFactory.checkUserExists(userData).then(function (response) {
                        console.log(response);
                        if (response.data.status == "success") {
                            reject("exist");
                        } else {
                            console.log('NOT');
                            resolve('does not exist');
                        }
                    });
                });
            }

            $scope.addFriend = function () {

                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                var reMob = /^[(]{0,1}[0-9]{3}[)]{0,1}[-\s\.]{0,1}[0-9]{3}[-\s\.]{0,1}[0-9]{4}$/;

                if ((!$scope.friend.mobNumber && !$scope.friend.email) || $scope.friend.email && !re.test($scope.friend.email) || $scope.friend.mobNumber && !reMob.test($scope.friend.mobNumber)) {
                    var validationInfo = "";


                    // if(!$scope.friend.firstName)
                    //  validationInfo += "<p>Please entry your First Name.</p>";
                    // if(!$scope.friend.lastName)
                    //  validationInfo += "<p>Please entry your Last Name.</p>";
                    if (($scope.friend.mobNumber == undefined || !$scope.friend.mobNumber.length) && !$scope.friend.email)
                        validationInfo += "<p>Please enter a friend's Mobile Number or Email ID.</p>";
                    if ($scope.friend.email && !re.test($scope.friend.email))
                        validationInfo += "<p>Please enter a valid email.</p>";
//                    if ($scope.friend.mobNumberValid && !reMob.test($scope.friend.mobNumber) || ($scope.friend.mobNumber != undefined && !$scope.friend.mobNumberValid))
//                        validationInfo += "<p>Please enter a valid phone number.</p>";

                    $ionicPopup.alert({
                        title: 'Please provide valid information',
                        template: validationInfo
                    });

                } else {

                    var data = {
                        "email": angular.lowercase($scope.friend.email),
                        "mobile": $scope.friend.mobNumber
                    };
                    $scope.friend.email = '';
                    $scope.friend.mobNumber = '';
                    console.log("USERNAME - " + GlobalServices.username)
                    // var friendExists = false;
                    //  for (var i=0; i<$scope.user.friends.length; i++) {
                    //    var f = $scope.user.friends[i];
                    //    if (f === id) {
                    //      friendExists = true;
                    //    }
                    //  }
                    // if (!friendExists) {
                    var userCheck = checkIfUserExist(data);
                    userCheck.then(function (success) {
                        console.log("promise", success);
                        var confirmPopup = $ionicPopup.confirm({
                            title: 'User does not exist',
                            template: 'User does not exist. Do you want to invite this user?',
                            cancelText: 'No',
                            okText: 'Yes'
                        });

                        confirmPopup.then(function (res) {
                            if (res) {
                                inviteFriend(data.email);
                            } else {
                                console.log('do nothing');
                            }
                        });

                    }, function (reason) {
                        console.log("promise", reason);

                        FriendFactory.addFriend(data).then(function (response) {
                            console.log(response);
                            console.log(response.status);
                            if (response.status == 200 && response.data.status == "success") {
                                $ionicPopup.alert({
                                    title: "Invite sent",
                                    template: "Your contact has been invited. Please wait for them to accept your invitation"
                                });
                                $state.go("main.friends");
                                initializeFriendList();
                                $scope.addFriendClose();
                            } else if (response.data.status == "error") {
                                // alert(response.data.message);
                                /*$ionicPopup.alert({
                                 title: "Error",
                                 template: response.data.message
                                 });*/

                            } else if (response.data == "Unauthorized") {
                                // alert("That username is already taken.");
                                $ionicPopup.alert({
                                    title: "Error",
                                    template: "That username is already taken."
                                });
                            }
                        });

                    });
                }

            };

            function inviteFriend(email) {
                var data = {email: email};
                FriendFactory.inviteExternalUser(data).then(function (response) {
                    console.log(response);
                    if (response.data.status == "success") {
                        $ionicPopup.alert({
                            title: "Success",
                            template: response.data.message
                        });
                    } else {
                        $ionicPopup.alert({
                            title: "Error",
                            template: response.data.message
                        });
                    }
                });
            }

            $ionicPopover.fromTemplateUrl('templates/groups-more-popover.html', {
                scope: $scope
            }).then(function (popover) {
                $scope.popover = popover;
            });

            $ionicModal.fromTemplateUrl('templates/add-friend.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modal = modal;
            });

            $ionicModal.fromTemplateUrl('templates/add-group.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalAddGroup = modal;
            });

            $ionicModal.fromTemplateUrl('templates/add-group-friends.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalAddGroupFriends = modal;
            });

            $ionicModal.fromTemplateUrl('templates/get-group-users.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalGetGroupUsers = modal;
            });

            $ionicModal.fromTemplateUrl('templates/remove-group-users.html', {
                scope: $scope,
                animation: 'slide-in-up',
                hardwareBackButtonClose: true
            }).then(function (modal) {
                $scope.modalRemoveGroupUsers = modal;
            });

            $scope.openPopover = function (event, gid) {
                $scope.groups.currentGroup = gid;
                FriendFactory.getGroupUsers({"user": GlobalServices.id, "gid": gid['_id']}).then(function (response) {
                    $scope.groups.currentGroupUsers = response.data;
                });
                if (!$scope.showPopover) {
                    $scope.popover.show(event);
                    $scope.showPopover = true;
                } else {
                    $scope.popover.hide();
                    $scope.showPopover = false;
                }
            };
            $scope.closePopover = function () {
                $scope.popover.hide();
            };

            $scope.removeGroupUsersOpen = function (group) {
                $scope.removeGroupUsersList = [];
                for (var friend in $scope.friendslist) {
                    for (var user in group.users) {
                        if ($scope.friendslist[friend]._id == group.users[user].id) {
                            $scope.removeGroupUsersList.push(group.users[user]);
                            break;
                        }
                    }
                }
                $scope.modalRemoveGroupUsers.show();
            };

            $scope.removeGroupUsersClose = function () {

                for (var i = 0; i < $scope.groups.currentGroup.users.length; i++) {
                    for (var j = 0; j < $scope.removeGroupUsersList.length; j++) {
                        if ($scope.removeGroupUsersList[j].remove === true && $scope.groups.currentGroup.users[i].id == $scope.removeGroupUsersList[j].id) {
                            $scope.groups.currentGroup.users.splice(i, 1);
                            i--;
                            break;
                        }
                    }
                }

                var data = {
                    "name": $scope.groups.currentGroup.name,
                    "type": $scope.groups.currentGroup.type,
                    "grpid": $scope.groups.currentGroup._id,
                    "owner": $scope.groups.currentGroup.owner,
                    "users": $scope.groups.currentGroup.users,
                    "edit": "true"
                };

                FriendFactory.addGroup(data).then(function (response) {
                    console.log(response);
                    $scope.modalRemoveGroupUsers.hide();
                    $scope.modalGetGroupUsers.hide();
                });
                $scope.removeGroupUsersList = [];
            };

            $scope.getGroupUsersOpen = function (gid) {
                $scope.groups.currentGroup = gid;
                FriendFactory.getGroupUsers({"user": GlobalServices.id, "gid": gid['_id']})
                        .then(function (response) {
                            console.log(response);
                            $scope.groups.currentGroupUsers = response.data;
                        });
                $scope.modalGetGroupUsers.show();
            };

            $scope.getGroupUsersClose = function () {
                $scope.popover.hide();
                $scope.modalGetGroupUsers.hide();
            };

            $scope.addFriendOpen = function () {
                $scope.modal.show();
            };

            $scope.addFriendClose = function () {
                $scope.modal.hide();
            };

            $scope.addGroupOpen = function (isEdit) {
                if (isEdit == 'edit') {
                    $scope.addGroupEdit = true;
                    $scope.groups.groupname = $scope.groups.currentGroup.name;
                    // switch ($scope.groups.currentGroup.type) {
                    //     case 'Friends':
                    //         $scope.groups.selectedGroupType = $scope.groups.groupTypes[0];
                    //         break;
                    //     case 'Family':
                    //         $scope.groups.selectedGroupType = $scope.groups.groupTypes[1];
                    //         break;
                    //     case 'Colleagues':
                    //         $scope.groups.selectedGroupType = $scope.groups.groupTypes[2];
                    //         break;
                    //     default:
                    //         $scope.groups.selectedGroupType = $scope.groups.groupTypes[3];
                    //         $scope.groups.customTypeName = $scope.groups.currentGroup.type;
                    // }
                    // ;
                }
                $scope.modalAddGroup.show();
            };

            $scope.addGroupClose = function () {
                $scope.groups.selectedGroupFriends = {};
                $scope.groups.groupname = "";
                $scope.addGroupEdit = false;
                $scope.popover.hide();
                $scope.modalAddGroup.hide();
            };
            $scope.addFriendsToGroup = function (groups) {
                $scope.groups.currentGroup = angular.copy(groups);
                $scope.isAddFriendsEdit = true;
                $scope.addFriendsList = [];
                for (var friend in $scope.friendslist) {
                    var key = true;
                    for (var user in $scope.groups.currentGroup.users) {
                        if ($scope.friendslist[friend]._id == $scope.groups.currentGroup.users[user].id) {
                            key = false;
                            break;
                        }
                    }
                    if (key)
                        $scope.addFriendsList.push($scope.friendslist[friend]);
                }
                $scope.modalAddGroupFriends.show();
            }
            $scope.addGroupFriendsOpen = function (isEdit) {
                $scope.isAddFriendsEdit = false;
                if (isEdit === 'edit') {
                    $scope.isAddFriendsEdit = true;
                    $scope.addFriendsList = [];

                    for (var friend in $scope.friendslist) {
                        var key = true;
                        for (var user in $scope.groups.currentGroup.users) {
                            if ($scope.friendslist[friend]._id == $scope.groups.currentGroup.users[user].id) {
                                key = false;
                                break;
                            }
                        }
                        if (key)
                            $scope.addFriendsList.push($scope.friendslist[friend]);
                    }
                }
                $scope.modalAddGroupFriends.show();
            };

            $scope.addGroupFriendsClose = function (isEdit) {
                if (isEdit == 'edit') {
                    var addedUsers = [];
                    for (var props in $scope.groups.selectedGroupFriends) {
                        if ($scope.groups.selectedGroupFriends[props] === true) {
                            for (var friend in $scope.friendslist) {
                                if ($scope.friendslist[friend]._id === props) {
                                    addedUsers.push({
                                        "id": $scope.friendslist[friend]._id,
                                        "name": $scope.friendslist[friend].name,
                                        "email": $scope.friendslist[friend].email
                                    });
                                    break;
                                }
                            }

                        }
                    }
                    $scope.groups.currentGroup.users = $scope.groups.currentGroup.users.concat(addedUsers);
                    var data = {
                        "name": $scope.groups.currentGroup.name,
                        "type": $scope.groups.currentGroup.type,
                        "grpid": $scope.groups.currentGroup._id,
                        "owner": $scope.groups.currentGroup.owner,
                        "users": $scope.groups.currentGroup.users,
                        "edit": "true"
                    };
                    FriendFactory.addGroup(data).then(function (response) {
                        console.log(response);
                        $scope.modalGetGroupUsers.hide();
                        $scope.modalAddGroupFriends.hide();
                        $scope.groups.selectedGroupFriends = {};

                    });
                } else
                    $scope.modalAddGroupFriends.hide();
            };
            var initializeFriendList = function () {
                FriendFactory.getFriends({"username": GlobalServices.username}).then(function (response) {
                    if (response.data) {
                        $scope.friendslist = response.data;
                    }
                });
            };
            initializeFriendList();

            $scope.$on('$destroy', function () {
                $scope.popover.remove();
                $scope.modal.remove();
                $scope.modalAddGroup.remove();
                $scope.modalGetGroupUsers.remove();
                $scope.modalAddGroupFriends.remove();
                $scope.modalRemoveGroupUsers.remove();
            });

            $scope.$on('popover.hidden', function () {
                $scope.showPopover = false;
            });

            //$scope.getFriends = function(){

            //};
            //$scope.getFriends();

        })
        .controller('NotificationsCtrl', function ($scope, $state, $ionicPopover, $ionicPopup, NotificationsFactory) {

            $scope.notificationIsRead = false;
            $scope.notifications = [];

            $ionicPopover.fromTemplateUrl('templates/notifications.html', {
                scope: $scope
            }).then(function (popover) {
                $scope.popover = popover;
            });

            $scope.openPopoverNotifications = function ($event) {
                $scope.popover.show($event);
                getNotifications();
            };
            $scope.closePopoverNotifications = function () {
                $scope.popover.hide();
            };



            function getNotifications() {
                NotificationsFactory.getNotifications().then(function (response) {
                    console.log(response);
                    if (response.status == 200) {
                        $scope.notifications = response.data;
                        console.log($scope.notifications);
                    } else {
                        $ionicPopup.alert({
                            title: "Error",
                            template: "Error occured while making request"
                        });
                    }
                });
            }

            $scope.markReadNotification = function (id) {
                var postData = {
                    nid: id
                }
                NotificationsFactory.markReadNotifications(postData).then(function (response) {
                    console.log(response);
                    if (response.status == 200) {
                        console.log('checked');
                    } else {
                        $ionicPopup.alert({
                            title: "Error",
                            template: "Error occured while checking notifications"
                        });
                    }
                });
                $scope.notificationIsRead = true;
            }

            $scope.markReadAllNotification = function (to) {
                var postData = {
                    all: to
                }
                NotificationsFactory.markReadNotifications(postData).then(function (response) {
                    console.log(response);
                    if (response.status == 200) {
                        console.log('checked');
                    } else {
                        $ionicPopup.alert({
                            title: "Error",
                            template: "Error occured while checking notifications"
                        });
                    }
                });
            }



        });
