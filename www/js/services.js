angular.module('starter.services', [])

        .factory('Chats', function () {
            // Might use a resource here that returns a JSON array

            // Some fake testing data
            var chats = [{
                    id: 0,
                    name: 'Ben Sparrow',
                    lastText: 'You on your way?',
                    face: 'https://pbs.twimg.com/profile_images/514549811765211136/9SgAuHeY.png'
                }, {
                    id: 1,
                    name: 'Max Lynx',
                    lastText: 'Hey, it\'s me',
                    face: 'https://avatars3.githubusercontent.com/u/11214?v=3&s=460'
                }, {
                    id: 2,
                    name: 'Adam Bradleyson',
                    lastText: 'I should buy a boat',
                    face: 'https://pbs.twimg.com/profile_images/479090794058379264/84TKj_qa.jpeg'
                }, {
                    id: 3,
                    name: 'Perry Governor',
                    lastText: 'Look at my mukluks!',
                    face: 'https://pbs.twimg.com/profile_images/598205061232103424/3j5HUXMY.png'
                }, {
                    id: 4,
                    name: 'Mike Harrington',
                    lastText: 'This is wicked good ice cream.',
                    face: 'https://pbs.twimg.com/profile_images/578237281384841216/R3ae1n61.png'
                }];

            return {
                all: function () {
                    return chats;
                },
                remove: function (chat) {
                    chats.splice(chats.indexOf(chat), 1);
                },
                get: function (chatId) {
                    for (var i = 0; i < chats.length; i++) {
                        if (chats[i].id === parseInt(chatId)) {
                            return chats[i];
                        }
                    }
                    return null;
                }
            };
        })
        .service('GlobalServices', function () {

            //this.apiurl = "http://54.165.210.229/api/";// our aws

            this.apiurl = "http://107.22.133.75/api/";// client's aws

            // this.apiurl = "http://localhost:10010/api/";

           // this.apiurl = "http://192.168.0.102:10010/api/";


            this.username = "";
            this.user_name = "";
            this.id = "";
            this.firstname = "";
            this.lastname = "";

            this.getDateFromDateTime = function(date){
                var d = date.getDate();
                var m = date.getMonth() + 1;
                var y = date.getFullYear();
                return (d <= 9 ? "0"+d.toString() : d.toString()) + "/" + (m <= 9 ? "0"+m.toString() : m.toString()) + "/" + y.toString();
            }

            this.getTimeZoneOffset = function(){// in millisecs
                return (new Date()).getTimezoneOffset() * 60000;
            }
        })
        .service('DateTimeServices', function (ionicTimePicker, ionicDatePicker) {
            this.openTimePicker = function(intialValue, callme){
                var options = {
                    callback: function (val) {
                      if (typeof (val) === 'undefined') {
                        console.log('Time not selected');
                        return undefined;
                      } else {
                        callme(val, (new Date(val * 1000)));// passes val(millisecs) of choosen time and date object with choosen time of the choosen date.
                      }
                    },
                    //inputTime: ((time % 15) == 0 ? time : time - (time % 15)),
                    inputTime: intialValue,
                    format: 12,
                    step: 15
                };
                ionicTimePicker.openTimePicker(options);
            };

            this.openDatePicker = function (intialValue, callme) {
                
                var options = {
                    callback: function (val) {  //Mandatory
                        callme(val, (new Date(val)));
                        // console.log('Return value from the datepicker popup is : ' + val, new Date(val));
                    },
                    from: new Date(),
                    //to: new Date(2016, 10, 30),
                    inputDate: intialValue,
                    mondayFirst: true,
                    //disableWeekdays: [0],
                    closeOnSelect: false,
                    templateType: 'popup'
                };
                ionicDatePicker.openDatePicker(options);
            };
        })
        .factory('AuthFactory', function ($http, GlobalServices, $q) {
            return{
                login: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "auth/login";
                    $http.post(url, data).then(function (response) {
                        this.username = response.data.username;
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                signup: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "auth/signup";
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    });
                    return deferred.promise;
                },
                verifyEmail: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "auth/verifyEmail";
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    });
                    return deferred.promise;
                },
                logout: function () {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "auth/logout";
                    $http.post(url).then(function (response) {
                        deferred.resolve(response);
                    });
                    return deferred.promise;
                },
                forgotPassword: function(data){
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "auth/forgotPassword";
                    $http.post(url, data).then(function(response){
                        deferred.resolve(response);
                    });
                    return deferred.promise;
                }
            }
        }).factory('FriendFactory', function ($http, GlobalServices, $q) {
    return{
        addFriend: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "friends/addFriend"
            console.log("url build " + url);
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        getFriends: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "friends/getFriends"
            console.log("url build " + url);
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        getUserGroups: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "groups/getUserGroups";
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        addGroup: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "groups/manipulateGroup";
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        getGroupUsers: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "groups/getGroupUsers";
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        removeGroup: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "groups/removeGroup";
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        acceptConnection: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "friends/acceptConnection";
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        skipConnection: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "friends/skipConnection";
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        removeConnection: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "friends/removeConnection";
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        checkUserExists: function(data){
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "friends/checkUserExists";
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        inviteExternalUser: function(data){
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "friends/inviteExternalUser";
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        }
    }
}).factory('PollFactory', function ($http, GlobalServices, $q) {
    return{
        setPoll: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "polls/setPoll"
            console.log("url build " + url);
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        getPolls: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "polls/getPolls"
            console.log("url build " + url);
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        updatePoll: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "polls/updatePoll"
            console.log("url build " + url);
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        deletePoll: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "polls/deletePoll"
            console.log("url build " + url);
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        savePollAnswers: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "polls/savePollAnswers"
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
        getFriendData: function (data) {
            var deferred = $q.defer();
            var url = GlobalServices.apiurl + "polls/getFriendData"
            $http.post(url, data).then(function (response) {
                deferred.resolve(response);
            }, function (error) {
                deferred.resolve(error);
            });
            return deferred.promise;
        },
    }
})

        .factory('NewsFeedFactory', function ($http, GlobalServices, $q) {
            return{
                getNews: function () {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "feedsettings/getAvailableFeeds"
                    $http.post(url).then(function (response) {

                        deferred.resolve(response);


                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                }
            }
        })
        .factory('UserProfileFactory', function ($http, GlobalServices, $q) {
            return{
                getUserProfile: function () {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "userprofile/getUserProfile"
                    $http.post(url).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                saveUserProfile: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "userprofile/saveUserProfile"
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                }
            }
        })
        .factory('PrivateMessages', function ($http, GlobalServices, $q) {
            return{
                getMessages: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "pmessages/getLatestMsgs"
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                sendMessage: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "pmessages/sendMessage"
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                getInitiatedChats: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "pmessages/getInitiatedChats"
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                markMessagesRead: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "pmessages/markMessagesRead"
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                }
            }
        })
        .factory('PostFactory', function ($http, GlobalServices, $q) {
            return{
                getPosts: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "posts/getPosts"
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                addPosts: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "posts/upsertPost"
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                sharePosts: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "posts/sharePost"
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                delComment: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "posts/delComment"
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                }
            }
        })

        .factory('EventFactory', function ($http, GlobalServices, $q) {
            return{
                addEvent: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "events/addEvent";
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                editEvent: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "events/editEvent";
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                getEvents: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "events/getEvents";
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                getEventDet: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "events/getEventDet";
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                acceptEventReq: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "events/acceptEventReq";
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                declineEventReq: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "events/declineEventReq";
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                subscribeEventReq: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "events/subscribeEventReq";
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                deleteEvent: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "events/deleteEvent";
                    console.log("url build " + url);
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
            }
        })

        .factory('NotificationsFactory', function ($http, GlobalServices, $q) {
            return{
                getNotifications: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "notifications/getNotifications";
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                },
                markReadNotifications: function (data) {
                    var deferred = $q.defer();
                    var url = GlobalServices.apiurl + "notifications/markRead";
                    $http.post(url, data).then(function (response) {
                        deferred.resolve(response);
                    }, function (error) {
                        deferred.resolve(error);
                    });
                    return deferred.promise;
                }
            }
        })

        /*.factory('PrivateMessages', function($http, GlobalServices, $q){
         return{
         getMessages: function(data){
         var deferred = $q.defer();
         var url = GlobalServices.apiurl + "pmessages/getLatestMsgs"
         $http.post(url, data).then(function(response){
         deferred.resolve(response);
         }, function(error) {
         deferred.resolve(error);
         });
         return deferred.promise;
         },
         sendMessage: function(data){
         var deferred = $q.defer();
         var url = GlobalServices.apiurl + "pmessages/sendMessage"
         $http.post(url, data).then(function(response){
         deferred.resolve(response);
         }, function(error) {
         deferred.resolve(error);
         });
         return deferred.promise;
         },
         getInitiatedChats: function(data){
         var deferred = $q.defer();
         var url = GlobalServices.apiurl + "pmessages/getInitiatedChats"
         $http.post(url, data).then(function(response){
         deferred.resolve(response);
         }, function(error) {
         deferred.resolve(error);
         });
         return deferred.promise;
         }
         }
         })*/
        .service('emojiService', function () {

            this.emojiListPeople = [':bowtie:', ':smile:', ' :laughing:'];
            this.emojiListNature = [':sunny:', ':umbrella:', ':cloud:'];
            this.emojiListObjects = [':bamboo:', ':gift_heart:', ':dolls:'];
            this.emojiListPlaces = [':house:', ':house_with_garden:', ':school:'];
            this.emojiListSymbols = [':one:', ':two:', ':three:'];

            this.emojis = [
                "bowtie", "smile", "laughing", "blush", "smiley", "relaxed",
                "smirk", "heart_eyes", "kissing_heart", "kissing_closed_eyes", "flushed",
                "relieved", "satisfied", "grin", "wink", "stuck_out_tongue_winking_eye",
                "stuck_out_tongue_closed_eyes", "grinning", "kissing",
                "kissing_smiling_eyes", "stuck_out_tongue", "sleeping", "worried",
                "frowning", "anguished", "open_mouth", "grimacing", "confused", "hushed",
                "expressionless", "unamused", "sweat_smile", "sweat",
                "disappointed_relieved", "weary", "pensive", "disappointed", "confounded",
                "fearful", "cold_sweat", "persevere", "cry", "sob", "joy", "astonished",
                "scream", "neckbeard", "tired_face", "angry", "rage", "triumph", "sleepy",
                "yum", "mask", "sunglasses", "dizzy_face", "imp", "smiling_imp",
                "neutral_face", "no_mouth", "innocent", "alien", "yellow_heart",
                "blue_heart", "purple_heart", "heart", "green_heart", "broken_heart",
                "heartbeat", "heartpulse", "two_hearts", "revolving_hearts", "cupid",
                "sparkling_heart", "sparkles", "star", "star2", "dizzy", "boom",
                "collision", "anger", "exclamation", "question", "grey_exclamation",
                "grey_question", "zzz", "dash", "sweat_drops", "notes", "musical_note",
                "fire", "hankey", "poop", "shit", "\\+1", "thumbsup", "-1", "thumbsdown",
                "ok_hand", "punch", "facepunch", "fist", "v", "wave", "hand", "raised_hand",
                "open_hands", "point_up", "point_down", "point_left", "point_right",
                "raised_hands", "pray", "point_up_2", "clap", "muscle", "metal", "fu",
                "walking", "runner", "running", "couple", "family", "two_men_holding_hands",
                "two_women_holding_hands", "dancer", "dancers", "ok_woman", "no_good",
                "information_desk_person", "raising_hand", "bride_with_veil",
                "person_with_pouting_face", "person_frowning", "bow", "couplekiss",
                "couple_with_heart", "massage", "haircut", "nail_care", "boy", "girl",
                "woman", "man", "baby", "older_woman", "older_man",
                "person_with_blond_hair", "man_with_gua_pi_mao", "man_with_turban",
                "construction_worker", "cop", "angel", "princess", "smiley_cat",
                "smile_cat", "heart_eyes_cat", "kissing_cat", "smirk_cat", "scream_cat",
                "crying_cat_face", "joy_cat", "pouting_cat", "japanese_ogre",
                "japanese_goblin", "see_no_evil", "hear_no_evil", "speak_no_evil",
                "guardsman", "skull", "feet", "lips", "kiss", "droplet", "ear", "eyes",
                "nose", "tongue", "love_letter", "bust_in_silhouette",
                "busts_in_silhouette", "speech_balloon", "thought_balloon", "feelsgood",
                "finnadie", "goberserk", "godmode", "hurtrealbad", "rage1", "rage2",
                "rage3", "rage4", "suspect", "trollface", "sunny", "umbrella", "cloud",
                "snowflake", "snowman", "zap", "cyclone", "foggy", "ocean", "cat", "dog",
                "mouse", "hamster", "rabbit", "wolf", "frog", "tiger", "koala", "bear",
                "pig", "pig_nose", "cow", "boar", "monkey_face", "monkey", "horse",
                "racehorse", "camel", "sheep", "elephant", "panda_face", "snake", "bird",
                "baby_chick", "hatched_chick", "hatching_chick", "chicken", "penguin",
                "turtle", "bug", "honeybee", "ant", "beetle", "snail", "octopus",
                "tropical_fish", "fish", "whale", "whale2", "dolphin", "cow2", "ram", "rat",
                "water_buffalo", "tiger2", "rabbit2", "dragon", "goat", "rooster", "dog2",
                "pig2", "mouse2", "ox", "dragon_face", "blowfish", "crocodile",
                "dromedary_camel", "leopard", "cat2", "poodle", "paw_prints", "bouquet",
                "cherry_blossom", "tulip", "four_leaf_clover", "rose", "sunflower",
                "hibiscus", "maple_leaf", "leaves", "fallen_leaf", "herb", "mushroom",
                "cactus", "palm_tree", "evergreen_tree", "deciduous_tree", "chestnut",
                "seedling", "blossom", "ear_of_rice", "shell", "globe_with_meridians",
                "sun_with_face", "full_moon_with_face", "new_moon_with_face", "new_moon",
                "waxing_crescent_moon", "first_quarter_moon", "waxing_gibbous_moon",
                "full_moon", "waning_gibbous_moon", "last_quarter_moon",
                "waning_crescent_moon", "last_quarter_moon_with_face",
                "first_quarter_moon_with_face", "moon", "earth_africa", "earth_americas",
                "earth_asia", "volcano", "milky_way", "partly_sunny", "octocat", "squirrel",
                "bamboo", "gift_heart", "dolls", "school_satchel", "mortar_board", "flags",
                "fireworks", "sparkler", "wind_chime", "rice_scene", "jack_o_lantern",
                "ghost", "santa", "christmas_tree", "gift", "bell", "no_bell",
                "tanabata_tree", "tada", "confetti_ball", "balloon", "crystal_ball", "cd",
                "dvd", "floppy_disk", "camera", "video_camera", "movie_camera", "computer",
                "tv", "iphone", "phone", "telephone", "telephone_receiver", "pager", "fax",
                "minidisc", "vhs", "sound", "speaker", "mute", "loudspeaker", "mega",
                "hourglass", "hourglass_flowing_sand", "alarm_clock", "watch", "radio",
                "satellite", "loop", "mag", "mag_right", "unlock", "lock",
                "lock_with_ink_pen", "closed_lock_with_key", "key", "bulb", "flashlight",
                "high_brightness", "low_brightness", "electric_plug", "battery", "calling",
                "email", "mailbox", "postbox", "bath", "bathtub", "shower", "toilet",
                "wrench", "nut_and_bolt", "hammer", "seat", "moneybag", "yen", "dollar",
                "pound", "euro", "credit_card", "money_with_wings", "e-mail", "inbox_tray",
                "outbox_tray", "envelope", "incoming_envelope", "postal_horn",
                "mailbox_closed", "mailbox_with_mail", "mailbox_with_no_mail", "door",
                "smoking", "bomb", "gun", "hocho", "pill", "syringe", "page_facing_up",
                "page_with_curl", "bookmark_tabs", "bar_chart", "chart_with_upwards_trend",
                "chart_with_downwards_trend", "scroll", "clipboard", "calendar", "date",
                "card_index", "file_folder", "open_file_folder", "scissors", "pushpin",
                "paperclip", "black_nib", "pencil2", "straight_ruler", "triangular_ruler",
                "closed_book", "green_book", "blue_book", "orange_book", "notebook",
                "notebook_with_decorative_cover", "ledger", "books", "bookmark",
                "name_badge", "microscope", "telescope", "newspaper", "football",
                "basketball", "soccer", "baseball", "tennis", "8ball", "rugby_football",
                "bowling", "golf", "mountain_bicyclist", "bicyclist", "horse_racing",
                "snowboarder", "swimmer", "surfer", "ski", "spades", "hearts", "clubs",
                "diamonds", "gem", "ring", "trophy", "musical_score", "musical_keyboard",
                "violin", "space_invader", "video_game", "black_joker",
                "flower_playing_cards", "game_die", "dart", "mahjong", "clapper", "memo",
                "pencil", "book", "art", "microphone", "headphones", "trumpet", "saxophone",
                "guitar", "shoe", "sandal", "high_heel", "lipstick", "boot", "shirt",
                "tshirt", "necktie", "womans_clothes", "dress", "running_shirt_with_sash",
                "jeans", "kimono", "bikini", "ribbon", "tophat", "crown", "womans_hat",
                "mans_shoe", "closed_umbrella", "briefcase", "handbag", "pouch", "purse",
                "eyeglasses", "fishing_pole_and_fish", "coffee", "tea", "sake",
                "baby_bottle", "beer", "beers", "cocktail", "tropical_drink", "wine_glass",
                "fork_and_knife", "pizza", "hamburger", "fries", "poultry_leg",
                "meat_on_bone", "spaghetti", "curry", "fried_shrimp", "bento", "sushi",
                "fish_cake", "rice_ball", "rice_cracker", "rice", "ramen", "stew", "oden",
                "dango", "egg", "bread", "doughnut", "custard", "icecream", "ice_cream",
                "shaved_ice", "birthday", "cake", "cookie", "chocolate_bar", "candy",
                "lollipop", "honey_pot", "apple", "green_apple", "tangerine", "lemon",
                "cherries", "grapes", "watermelon", "strawberry", "peach", "melon",
                "banana", "pear", "pineapple", "sweet_potato", "eggplant", "tomato", "corn",
                "house", "house_with_garden", "school", "office", "post_office", "hospital",
                "bank", "convenience_store", "love_hotel", "hotel", "wedding", "church",
                "department_store", "european_post_office", "city_sunrise", "city_sunset",
                "japanese_castle", "european_castle", "tent", "factory", "tokyo_tower",
                "japan", "mount_fuji", "sunrise_over_mountains", "sunrise", "stars",
                "statue_of_liberty", "bridge_at_night", "carousel_horse", "rainbow",
                "ferris_wheel", "fountain", "roller_coaster", "ship", "speedboat", "boat",
                "sailboat", "rowboat", "anchor", "rocket", "airplane", "helicopter",
                "steam_locomotive", "tram", "mountain_railway", "bike", "aerial_tramway",
                "suspension_railway", "mountain_cableway", "tractor", "blue_car",
                "oncoming_automobile", "car", "red_car", "taxi", "oncoming_taxi",
                "articulated_lorry", "bus", "oncoming_bus", "rotating_light", "police_car",
                "oncoming_police_car", "fire_engine", "ambulance", "minibus", "truck",
                "train", "station", "train2", "bullettrain_front", "bullettrain_side",
                "light_rail", "monorail", "railway_car", "trolleybus", "ticket", "fuelpump",
                "vertical_traffic_light", "traffic_light", "warning", "construction",
                "beginner", "atm", "slot_machine", "busstop", "barber", "hotsprings",
                "checkered_flag", "crossed_flags", "izakaya_lantern", "moyai",
                "circus_tent", "performing_arts", "round_pushpin",
                "triangular_flag_on_post", "jp", "kr", "cn", "us", "fr", "es", "it", "ru",
                "gb", "uk", "de", "one", "two", "three", "four", "five", "six", "seven",
                "eight", "nine", "keycap_ten", "1234", "zero", "hash", "symbols",
                "arrow_backward", "arrow_down", "arrow_forward", "arrow_left",
                "capital_abcd", "abcd", "abc", "arrow_lower_left", "arrow_lower_right",
                "arrow_right", "arrow_up", "arrow_upper_left", "arrow_upper_right",
                "arrow_double_down", "arrow_double_up", "arrow_down_small",
                "arrow_heading_down", "arrow_heading_up", "leftwards_arrow_with_hook",
                "arrow_right_hook", "left_right_arrow", "arrow_up_down", "arrow_up_small",
                "arrows_clockwise", "arrows_counterclockwise", "rewind", "fast_forward",
                "information_source", "ok", "twisted_rightwards_arrows", "repeat",
                "repeat_one", "new", "top", "up", "cool", "free", "ng", "cinema", "koko",
                "signal_strength", "u5272", "u5408", "u55b6", "u6307", "u6708", "u6709",
                "u6e80", "u7121", "u7533", "u7a7a", "u7981", "sa", "restroom", "mens",
                "womens", "baby_symbol", "no_smoking", "parking", "wheelchair", "metro",
                "baggage_claim", "accept", "wc", "potable_water", "put_litter_in_its_place",
                "secret", "congratulations", "m", "passport_control", "left_luggage",
                "customs", "ideograph_advantage", "cl", "sos", "id", "no_entry_sign",
                "underage", "no_mobile_phones", "do_not_litter", "non-potable_water",
                "no_bicycles", "no_pedestrians", "children_crossing", "no_entry",
                "eight_spoked_asterisk", "eight_pointed_black_star", "heart_decoration",
                "vs", "vibration_mode", "mobile_phone_off", "chart", "currency_exchange",
                "aries", "taurus", "gemini", "cancer", "leo", "virgo", "libra", "scorpius",
                "sagittarius", "capricorn", "aquarius", "pisces", "ophiuchus",
                "six_pointed_star", "negative_squared_cross_mark", "a", "b", "ab", "o2",
                "diamond_shape_with_a_dot_inside", "recycle", "end", "on", "soon", "clock1",
                "clock130", "clock10", "clock1030", "clock11", "clock1130", "clock12",
                "clock1230", "clock2", "clock230", "clock3", "clock330", "clock4",
                "clock430", "clock5", "clock530", "clock6", "clock630", "clock7",
                "clock730", "clock8", "clock830", "clock9", "clock930", "heavy_dollar_sign",
                "copyright", "registered", "tm", "x", "heavy_exclamation_mark", "bangbang",
                "interrobang", "o", "heavy_multiplication_x", "heavy_plus_sign",
                "heavy_minus_sign", "heavy_division_sign", "white_flower", "100",
                "heavy_check_mark", "ballot_box_with_check", "radio_button", "link",
                "curly_loop", "wavy_dash", "part_alternation_mark", "trident",
                "black_square", "white_square", "white_check_mark", "black_square_button",
                "white_square_button", "black_circle", "white_circle", "red_circle",
                "large_blue_circle", "large_blue_diamond", "large_orange_diamond",
                "small_blue_diamond", "small_orange_diamond", "small_red_triangle",
                "small_red_triangle_down", "shipit"
            ];

        })
        .service('errorMsg', ['$ionicPopup', '$q', function ($ionicPopup, $q) {
                return {
                    showError: function (tpl) {
//                        var deferred = $q.defer();
                        var confirmPopup = $ionicPopup.confirm({
                            title: 'Errors',
                            template: tpl
                        });
//                        deferred.resolve(error);
                    }
                }


        }]).directive('ngEnter', function() {
        return function(scope, element, attrs) {
            element.bind("keydown keypress", function(event) {
                if(event.which === 13) {
                        scope.$apply(function(){
                                scope.$eval(attrs.ngEnter);
                        });
                        
                        event.preventDefault();
                }
            });
        };
});

//                        return deferred.promise;
//                    }
//                };
//            }]);

