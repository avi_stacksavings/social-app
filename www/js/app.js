
angular.module('starter', ['ionic',
    'chart.js',
    'starter.controllers',
    'starter.filters',
    'starter.services',
    'ion-google-place',
    'ngFileUpload',
    'ion-autocomplete',
    'naif.base64',
    'ngLoadingSpinner',
    'emoji',
    'ngCordova',
    'ui.mask',
    'ionic-datepicker',
    'ionic-timepicker'
])
        .run(function ($ionicPlatform) {
            $ionicPlatform.ready(function () {
                if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

                }
                if (window.StatusBar) {
                    // org.apache.cordova.statusbar required
                    StatusBar.styleLightContent();
                }
            });

        })

        .config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $httpProvider) {
            $httpProvider.defaults.withCredentials = true;
            // Ionic uses AngularUI Router which uses the concept of states
            // Learn more here: https://github.com/angular-ui/ui-router
            // Set up the various states which the app can be in.
            // Each state's controller can be found in controllers.js
            $stateProvider

                    // Login/Signup
                    .state('login', {
                        url: '/login',
                        // views: {
                        //   'tab-dash': {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl',
                        resolve: {
                            hideNavbar: function ($ionicNavBarDelegate) {
                                $ionicNavBarDelegate.showBar(false);
                                return true;
                            }
                        }
                        // }
                        // }
                    })
                    // .state('signup', {
                    //   url: '/signup',
                    //   // views: {
                    //   //   'tab-dash': {
                    //       templateUrl: 'templates/signup.html',
                    //       controller: 'SignupCtrl'
                    //     // }
                    //   // }
                    // })
                    // setup an abstract state for the tabs directive
                    .state('main', {
                        url: "/main",
                        abstract: true,
                        templateUrl: "templates/main.html",
                        controller: 'MainController'
                    })
                    //   .state('main.tab', {
                    //   url: "/tab",
                    //   // abstract: true,
                    //   templateUrl: "templates/tabs.html"
                    // })

                    // Each tab has its own nav history stack:

                    .state('main.dash', {
                        url: '/dash',
                        views: {
                            'main-dash': {
                                templateUrl: 'templates/tab-dash.html',
                                controller: 'DashCtrl'
                            }
                        }
                    })

                    .state('main.account', {
                        url: '/account',
                        views: {
                            'main-account': {
                                templateUrl: 'templates/tab-account.html',
                                controller: 'AccountCtrl'
                            }
                        }
                    })

                    .state('main.settings', {
                        url: '/settings',
                        views: {
                            'main-settings': {
                                templateUrl: 'templates/tab-settings.html',
                                controller: 'SettingsCtrl'
                            }
                        }
                    })
                    .state('main.menu', {
                        url: '/menu',
                        views: {
                            'main-menu': {
                                templateUrl: 'templates/tab-main.html',
                                controller: 'MainMenuCtrl'
                            }
                        }
                    })
                    .state('main.polls', {
                        url: '/polls',
                        params: {page: null},
                        views: {
                            'tab-polls': {
                                templateUrl: 'templates/tab-polls.html',
                                controller: 'PollsCtrl'
                            }
                        },
                        cache: false
                    })
                    .state('main.more', {
                        url: '/more',
                        views: {
                            'main-more': {
                                templateUrl: 'templates/tab-more.html',
                                // controller: 'MoreCtrl'
                            }
                        }
                    })
                    .state('main.plan', {
                        url: '/plan',
                        views: {
                            'main-plan': {
                                templateUrl: 'templates/tab-plan.html',
                                controller: 'PlanCtrl'
                            }
                        },
                        cache: false
                    })
                    .state('main.vote', {
                        url: '/vote',
                        views: {
                            'main-vote': {
                                templateUrl: 'templates/tab-vote.html',
                                // controller: 'MoreCtrl'
                            }
                        }
                    })
                    .state('main.interact', {
                        url: '/interact',
                        views: {
                            'main-interact': {
                                templateUrl: 'templates/tab-interact.html',
                                controller: 'InteractCtrl'
                            }
                        },
                        cache: false
                    })
                    .state('main.post', {
                        url: '/post',
                        views: {
                            'main-interact': {
                                templateUrl: 'templates/post.html',
                                controller: 'PostCtrl'
                            }
                        },
                        cache: false
                    })
                    .state('main.comment', {
                        url: '/comment',
                        views: {
                            'main-interact': {
                                templateUrl: 'templates/comments.html',
                                controller: 'CommentCtrl'
                            }
                        },
                        cache: false
                    })
                    .state('main.friends', {
                        url: '/friends',
                        views: {
                            'main-friends': {
                                templateUrl: 'templates/tab-friends.html',
                                controller: 'FriendsCtrl'
                            }
                        },
                        cache: false
                    })
//                    .state('main.connection', {
//                        url: '/connection',
//                        views: {
//                            'main-friends': {
//                                templateUrl: 'templates/tab-connection.html',
//                                controller: 'ConnectionCtrl'
//                            }
//                        },
//                        cache: false
//                    })
                    .state('main.userprofile', {
                        url: '/userprofile',
                        views: {
                            'main-userprofile': {
                                templateUrl: 'templates/tab-userprofile.html',
                                controller: 'UserProfileCtrl'
                            }
                        },
                        cache: false
                    })
                    .state('main.invites', {
                        url: '/invites',
                        views: {
                            'main-invites': {
                                templateUrl: 'templates/tab-invites.html',
                                controller: 'InviteCtrl'
                            }
                        }
                    })
                    .state('main.groups', {
                        url: '/groups',
                        views: {
                            'main-groups': {
                                templateUrl: 'templates/tab-groups.html',
                                controller: 'GroupCtrl'
                            }
                        }
                    })
                    .state('main.newssettings', {
                        url: '/newssettings',
                        views: {
                            'main-newssettings': {
                                templateUrl: 'templates/tab-newssettings.html',
                                controller: 'NewsSettingsCtrl'
                            }
                        }
                    });
            //   .state('main.settings', {
            //     url: '/settings',
            //     views: {
            //       'main-settings': {
            //         templateUrl: 'templates/tab-settings.html',
            //         controller: 'SettingsCtrl'
            //       }
            //   }
            // });


            // if none of the above states are matched, use this as the fallback
            $urlRouterProvider.otherwise('/login');


            // Ionic Config
            $ionicConfigProvider.tabs.position("bottom")
            

        });



//angular.module('starter').directive('qyToggleOverflowScroll', qyToggleOverflowScroll);
/* @ngInject */
function qyToggleOverflowScroll($timeout, $window, qyIonicReady, ionic) {
    var directive = {
        restrict: 'A',
        link: link
    };
    return directive;

    function link(scope, element, attrs) {
        var domElement = element[0];

        qyIonicReady().then(function onQyIonicReady() {
            $window.addEventListener('native.keyboardshow', handleKeyboardShow);
            $window.addEventListener('native.keyboardhide', handleKeyboardHide);

            // remove event listener on destroy
            scope.$on('$destroy', removeKeyboardHandlerListener);

            function handleKeyboardShow() {
                console.log('qyOverflowScrollToggle keyboard show: ELEMENT CLASS LIST: '
                        + domElement.classList.toString());
                // iOS or Android full screen
                var isIosOrAndroidFullScreen
                        = ionic.Platform.isIOS() || (ionic.Platform.isAndroid() && ionic.Platform.isFullScreen);

                if (isIosOrAndroidFullScreen) {
                    console.log('qyOverflowScrollToggle: '
                            + 'keyboard is shown, set overflow-y to: scroll');
                    domElement.style.overflowY = 'hidden';
                    // set -webkit-overflow-scrolling to auto for having non-momentum scrolling if
                    // keyboard is up. Setting it to touch causes screen flicker when closing keyboard
                    domElement.style.webkitOverflowScrolling = 'auto';

                    $timeout(function setOverflowYToScrollIfNeeded() {
                        var scrollerHeight = element.height();
                        var scrollerContentHeight = domElement.scrollHeight;

                        // if scroller contains enough content to enable scrolling
                        if (scrollerContentHeight > scrollerHeight + 1) {
                            console.log('qyOverflowScrollToggle keyboard show: '
                                    + 'scroller height / scroller content height: '
                                    + scrollerHeight
                                    + ' / '
                                    + scrollerContentHeight);

                            console.log('qyOverflowScrollToggle keyboard show: '
                                    + 'content larger than scroller, set overflow-y to: scroll');
                            domElement.style.overflowY = 'scroll';
                            // no need to set -webkit-overflow-scrolling as it should remain with value auto
                            // whenever keyboard is up. We disable momentum scrolling when keyboard is up.
                        }
                    }, 400);
                }
            }

            function handleKeyboardHide() {
                console.log('qyOverflowScrollToggle keyboard hide: ELEMENT CLASS LIST: '
                        + domElement.classList.toString());
                //// iOS or Android full screen
                var isIosOrAndroidFullScreen
                        = ionic.Platform.isIOS() || (ionic.Platform.isAndroid() && ionic.Platform.isFullScreen);

                if (isIosOrAndroidFullScreen) {
                    domElement.style.overflowY = 'hidden';
                    // set -webkit-overflow-scrolling to auto for keyboard transition
                    domElement.style.webkitOverflowScrolling = 'auto';

                    $timeout(function setOverflowYToScrollIfNeeded() {
                        var scrollerHeight = domElement.clientHeight;
                        var scrollerContentHeight = domElement.scrollHeight;

                        console.log('qyOverflowScrollToggle keyboard hide: '
                                + 'scroller height / scroller content height: '
                                + scrollerHeight
                                + ' / '
                                + scrollerContentHeight);

                        // if scroller contains enough content to enable scrolling
                        if (scrollerContentHeight > scrollerHeight + 1) {
                            console.log('qyOverflowScrollToggle keyboard hide: '
                                    + 'content larger than scroller, set overflow-y to: scroll');
                            domElement.style.overflowY = 'scroll';
                            // set -webkit-overflow-scrolling to touch for default momentum scrolling if
                            // keyboard is not up
                            domElement.style.webkitOverflowScrolling = 'touch';
                        }
                    }, 400);
                }
            }

            function removeKeyboardHandlerListener() {
                $window.removeEventListener('native.keyboardshow', handleKeyboardShow);
                $window.removeEventListener('native.keyboardhide', handleKeyboardHide);
            }
        });
    }
}
angular.module('starter').factory('qyIonicReady', qyIonicReady);

/**
 * Workaround for silently ignored callbacks.
 *
 * Details see http://stealthcode.co/multiple-calls-to-ionicplatform-ready/
 */
/* @ngInject */
function qyIonicReady($ionicPlatform) {
    var readyPromise;

    return function () {
        if (!readyPromise) {
            readyPromise = $ionicPlatform.ready();
        }
        return readyPromise;
    };
}