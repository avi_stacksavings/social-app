# Building application guide (Android version)

    * cordova build --release android
    * jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore stack-savings.keystore  [root folder]/platforms/android/build/outputs/apk/android-release-unsigned.apk stack-savings
    * [android sdk root folder]/sdk/build-tools/21.1.2/zipalign -v 4 [root folder]/platforms/android/build/outputs/apk/android-release-unsigned.apk socialApp.apk